package ru.rencredit.volkov.reminder;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class DiscoveryService {

    public static void main(@Nullable final String[] args) {
        SpringApplication.run(DiscoveryService.class, args);
    }

}