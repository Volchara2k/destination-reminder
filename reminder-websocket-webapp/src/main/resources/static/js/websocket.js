const URL_LIST = "ws://localhost:8094/destinations";

const URL_CREATE = "ws://localhost:8094/create";

const URL_DELETE = "ws://localhost:8094/delete";

const LIST_WEB_SOCKET = new WebSocket(URL_LIST);

const CREATE_WEB_SOCKET = new WebSocket(URL_CREATE);

const DELETE_WEB_SOCKET = new WebSocket(URL_DELETE);

const EMAILS = [];

LIST_WEB_SOCKET.onmessage = function (event) {
    joinData(event.data);
};

CREATE_WEB_SOCKET.onmessage = function (event) {
    joinData(event.data);
}

DELETE_WEB_SOCKET.onmessage = function (event) {
    disposeData(event.data);
}

function joinData(email) {
    console.log("Join email: " + email);
    EMAILS.push(email);
    return drawData(email);
}

function disposeData(email) {
    console.log("Dispose email: " + email);
    document.getElementById(email).remove();
    let index = EMAILS.indexOf(email);
    if (index > -1) {
        EMAILS.splice(index, 1);
    }
}

function drawData(email) {
    document.querySelector(".list-table").innerHTML +=
        '<tr id="' + email + '">' +
        '<td>' + email + '</td>' +
        '<td class="text-right">' +
        '<div class="btn-group pull-right">' +
        '<button class="btn btn-outline-danger btn-sm"  onclick="deleteDestination(this);" type="button" name="' + email + '">Удалить</button>' +
        '</div>' +
        '</td>' +
        '</tr>';
}

function createDestination() {
    let email = document.getElementById("email").value;
    let invalidEmail = isInvalidEmail(email);
    if (invalidEmail) {
        return drawFailure();
    }

    let incorrectEmail = !isCorrectEmail(email);
    if (incorrectEmail) {
        return drawIncorrectEmail();
    }

    CREATE_WEB_SOCKET.send(email);
    return drawSuccess();
}

function isInvalidEmail(email) {
    if (email == null || email === "") return true;
    return EMAILS.includes(email);
}

function isCorrectEmail(email) {
    const regExp = /([a-zA-Z0-9]+)([.{1}])?([a-zA-Z0-9]+)@rencredit([.])ru/;
    return regExp.test(email);
}

function drawIncorrectEmail() {
    document.getElementById("response").innerHTML =
        '<span style=color:red;>' +
        'Ошибка отправки. Проверьте корректность электронной почты' +
        '</span>';
}

function drawFailure() {
    document.getElementById("response").innerHTML =
        '<span style=color:red;>' +
        'Ошибка отправки. Проверьте корректность данных' +
        '</span>';
}

function drawSuccess() {
    document.getElementById("response").innerHTML =
        '<span style=color:dodgerblue;>' +
        'Успешно!' +
        '</span>';
}

function deleteDestination(assignedButton) {
    let email = assignedButton.name;
    DELETE_WEB_SOCKET.send(email);
    return drawSuccess();
}