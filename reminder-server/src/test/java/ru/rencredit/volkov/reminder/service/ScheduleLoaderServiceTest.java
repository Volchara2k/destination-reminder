package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IScheduleLoaderService;
import ru.rencredit.volkov.reminder.casedata.StringCaseDataProvider;
import ru.rencredit.volkov.reminder.exception.InvalidLoadScheduleException;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;

import java.util.UUID;

@Setter
public class ScheduleLoaderServiceTest extends AbstractTest {

    @Autowired
    private IScheduleLoaderService scheduleLoaderService;

    @Test
    @IntegrationImplementation
    public void negativeSpecLoadToResourceTest() {
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(invalidPathLine ->
                Assertions.assertThrows(InvalidParamException.class,
                        () -> this.scheduleLoaderService.loadToResource(invalidPathLine, UUID.randomUUID().toString())));
    }

    @Test
    @IntegrationImplementation
    public void negativePathnameLoadToResourceTest() {
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(invalidPathLine ->
                Assertions.assertThrows(InvalidParamException.class,
                        () -> this.scheduleLoaderService.loadToResource(UUID.randomUUID().toString(), invalidPathLine)));
    }

    @Test
    @IntegrationImplementation
    public void negativeCopyFileFromURLTest() {
        Assertions.assertThrows(InvalidLoadScheduleException.class,
                () -> this.scheduleLoaderService.loadToResource(UUID.randomUUID().toString(), UUID.randomUUID().toString()));
    }

}