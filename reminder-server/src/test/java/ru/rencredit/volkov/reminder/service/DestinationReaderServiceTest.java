package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IDestinationReaderService;
import ru.rencredit.volkov.reminder.casedata.StringCaseDataProvider;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.domain.pojo.DestinationPOJO;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import java.util.Collection;

@Setter
public class DestinationReaderServiceTest extends AbstractTest {

    @Autowired
    private IDestinationReaderService destinationReaderService;

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void negativeReadFromResourcesTest() {
        Assertions.assertNotNull(this.destinationReaderService);
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(invalidPathLine ->
                Assertions.assertThrows(InvalidParamException.class,
                        () -> this.destinationReaderService.readFromResources(invalidPathLine)));
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void readFromResourcesTest() {
        Assertions.assertNotNull(this.destinationReaderService);
        @NotNull final Collection<DestinationPOJO> schedules = this.destinationReaderService.readFromResources();
        Assertions.assertNotNull(schedules);
        Assertions.assertNotEquals(0, schedules.size());
        schedules.forEach(schedule -> {
            Assertions.assertFalse(ValidRuleUtil.isNullOrEmpty(schedule.getId()));
            Assertions.assertFalse(ValidRuleUtil.isNullOrEmpty(schedule.getEmail()));
        });
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void readFromResourcesByPathnameTest() {
        Assertions.assertNotNull(this.destinationReaderService);
        @NotNull final Collection<DestinationPOJO> schedules = this.destinationReaderService.readFromResources("destination.csv");
        Assertions.assertNotNull(schedules);
        Assertions.assertNotEquals(0, schedules.size());
        schedules.forEach(schedule -> {
            Assertions.assertFalse(ValidRuleUtil.isNullOrEmpty(schedule.getId()));
            Assertions.assertFalse(ValidRuleUtil.isNullOrEmpty(schedule.getEmail()));
        });
    }

}