package ru.rencredit.volkov.reminder.util;

import org.apache.commons.lang3.StringUtils;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class EmailBuilderUtil {

    @NotNull
    public String buildFrom(@NotNull final String header) {
        return StringUtils.join(header, "@rencredit.ru");
    }

}