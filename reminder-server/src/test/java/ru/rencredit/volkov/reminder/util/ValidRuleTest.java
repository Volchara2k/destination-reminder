package ru.rencredit.volkov.reminder.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.rencredit.volkov.reminder.casedata.StringCaseDataProvider;
import ru.rencredit.volkov.reminder.marker.PositiveImplementation;

public class ValidRuleTest {

    @Test
    @PositiveImplementation
    public void isNullOrEmptyStringTest() {
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(string -> {
            final boolean isNullOrEmpty = ValidRuleUtil.isNullOrEmpty(string);
            Assertions.assertTrue(isNullOrEmpty);
        });
    }

}