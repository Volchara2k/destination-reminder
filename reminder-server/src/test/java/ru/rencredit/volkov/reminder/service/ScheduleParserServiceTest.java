package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IScheduleParserService;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

@Setter
public class ScheduleParserServiceTest extends AbstractTest {

    @Autowired
    private IScheduleParserService scheduleParserService;

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void parseRestDaysByMonthOfYearTest() {
        @NotNull final String[] restDays = this.scheduleParserService.parseRestDaysByMonthOfYear(12, 2000);
        final boolean isNotExistsRestDays = ValidRuleUtil.isNullOrEmpty(restDays);
        Assertions.assertFalse(isNotExistsRestDays);
    }

}