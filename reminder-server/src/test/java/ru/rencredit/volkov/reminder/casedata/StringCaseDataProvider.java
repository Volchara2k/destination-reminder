package ru.rencredit.volkov.reminder.casedata;

import org.jetbrains.annotations.NotNull;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class StringCaseDataProvider {

    @NotNull
    public static final Supplier<Stream<String>> INVALID_STRING_STREAM = () -> Stream.of(
            "", "   ", "                                   ", null
    );

}