package ru.rencredit.volkov.reminder;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@Setter
@WebAppConfiguration
public class AbstractWebTest extends AbstractTest{

    @NotNull
    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @NotNull
    protected static ObjectWriter OBJECT_WRITER;

    @NotNull
    @Autowired
    private WebApplicationContext webApplicationContext;

    @NotNull
    protected MockMvc mockMvc;

    @BeforeAll
    public static void createMapperBeforeClass() {
        OBJECT_MAPPER.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        OBJECT_WRITER = OBJECT_MAPPER.writer().withDefaultPrettyPrinter();
    }

    @BeforeEach
    public void loadContextBefore() {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();
    }

}