package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IScheduleListenerService;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;

@Setter
public class ScheduleListenerServiceTest extends AbstractTest {

    @Autowired
    private IScheduleListenerService scheduleListenerService;

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void todayIsDispatchDayTest() {
        final boolean isDispatchDay = this.scheduleListenerService.todayIsDispatchDay(14, 1, 2011);
        Assertions.assertTrue(isDispatchDay);
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void todayIsNotDispatchDayTest() {
        final boolean isDispatchDay = this.scheduleListenerService.todayIsDispatchDay(1, 1, 1999);
        Assertions.assertFalse(isDispatchDay);
    }

}