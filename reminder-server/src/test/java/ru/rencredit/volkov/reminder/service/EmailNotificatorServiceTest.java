package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IEmailNotificatorService;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.util.EmailBuilderUtil;

import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Setter
public class EmailNotificatorServiceTest extends AbstractTest {

    @Autowired
    private IEmailNotificatorService emailNotificatorService;

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void negativeNotifySubscribersTest() {
        Assertions.assertNotNull(this.emailNotificatorService);
        Assertions.assertThrows(InvalidAddresseeException.class,
                () -> this.emailNotificatorService.notifySubscribers(null)
        );
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void notifySubscribersTest() {
        Assertions.assertNotNull(this.emailNotificatorService);
        final boolean isNotifySubs = this.emailNotificatorService.notifySubscribers();
        Assertions.assertTrue(isNotifySubs);
    }


    @NotNull
    private static final Stream<String> SUBSCRIBER_STREAM = Stream.of(
            EmailBuilderUtil.buildFrom(UUID.randomUUID().toString()),
            EmailBuilderUtil.buildFrom(UUID.randomUUID().toString()),
            EmailBuilderUtil.buildFrom(UUID.randomUUID().toString())
    );

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void notifySubscribersByCollectionTest() {
        Assertions.assertNotNull(this.emailNotificatorService);
        final boolean isNotifySubs = this.emailNotificatorService.notifySubscribers(
                SUBSCRIBER_STREAM.collect(Collectors.toList())
        );
        Assertions.assertTrue(isNotifySubs);
    }

}