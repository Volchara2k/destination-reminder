package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IScheduleService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.io.IOException;
import java.util.Collection;
import java.util.function.Supplier;
import java.util.stream.Stream;

@Setter
public class ScheduleServiceTest extends AbstractTest {

    @Autowired
    private IScheduleService scheduleService;

    @NotNull
    private static final Supplier<Stream<Integer>> INVALID_YEARS_STREAM = () -> Stream.of(
            1, 2, 3, 10, -600, 1000, 1337, -999
    );

    @Test
    @IntegrationImplementation
    public void negativeGetScheduleByYearTest() {
        Assertions.assertNotNull(this.scheduleService);
        INVALID_YEARS_STREAM.get().forEach(invalidYear ->
                Assertions.assertThrows(InvalidScheduleYearException.class,
                        () -> this.scheduleService.getScheduleByYear(invalidYear)));
    }

    @NotNull
    private static final Supplier<Stream<Integer>> VALID_YEARS_STREAM = () -> Stream.of(
            2019, 2020, 2021, 2022, 1999, 2000, 2010, 2014
    );

    @Test
    @IntegrationImplementation
    public void getScheduleByYearTest() {
        Assertions.assertNotNull(this.scheduleService);
        VALID_YEARS_STREAM.get().forEach(validYear -> {
            try {
                @NotNull final RestSchedulePOJO schedule = this.scheduleService.getScheduleByYear(validYear);
                Assertions.assertNotNull(schedule);
            } catch (InvalidParamException | IOException | InvalidScheduleYearException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void getSchedulesTest() {
        Assertions.assertNotNull(this.scheduleService);
        @NotNull final Collection<RestSchedulePOJO> schedules = this.scheduleService.getAllSchedules();
        Assertions.assertNotNull(schedules);
        Assertions.assertNotEquals(0, schedules.size());
    }

}