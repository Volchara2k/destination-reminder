package ru.rencredit.volkov.reminder.endpoint.rest;

import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.rencredit.volkov.reminder.AbstractWebTest;
import ru.rencredit.volkov.reminder.util.EmailBuilderUtil;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Setter
public class DestinationRestEndpointTest extends AbstractWebTest {

    @NotNull
    private static final String BASE_URL = "/api/v1";

    @Test
    @SneakyThrows
    public void getAllDestinationsTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get(StringUtils.join(BASE_URL, "/destinations")))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @SneakyThrows
    public void getAllDestinationsMail() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get(StringUtils.join(BASE_URL, "/destinations/emails")))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @SneakyThrows
    public void subscribeDestination() {
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        @NotNull final String requestJson = OBJECT_WRITER.writeValueAsString(email);
        super.mockMvc.perform(MockMvcRequestBuilders
                .post(StringUtils.join(BASE_URL, "/destination/subscribe"))
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.email").value(String.format("\"%s\"", email)));
    }

    @Test
    @SneakyThrows
    public void unsubscribeDestination() {
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        super.mockMvc.perform(MockMvcRequestBuilders
                .delete(StringUtils.join(BASE_URL, "/destination/unsubscribe/{email}"), email))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful());
    }

}