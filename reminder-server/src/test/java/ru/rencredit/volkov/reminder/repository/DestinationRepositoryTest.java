package ru.rencredit.volkov.reminder.repository;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.domain.entity.Destination;
import ru.rencredit.volkov.reminder.util.EmailBuilderUtil;

import java.util.UUID;

@Setter
public class DestinationRepositoryTest extends AbstractTest {

    @Autowired
    private IDestinationRepository destinationRepository;

    @Test
    @IntegrationImplementation
    public void deleteByEmailTest() {
        Assertions.assertNotNull(this.destinationRepository);
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        Assertions.assertNotNull(email);
        @NotNull final Destination subscriber = this.destinationRepository.insert(new Destination(email));
        Assertions.assertNotNull(subscriber);
        int deleteSubscriberResponse = this.destinationRepository.deleteByEmail(email);
        Assertions.assertEquals(1, deleteSubscriberResponse);
    }

}