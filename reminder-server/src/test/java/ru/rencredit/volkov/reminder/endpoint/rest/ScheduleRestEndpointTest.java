package ru.rencredit.volkov.reminder.endpoint.rest;

import lombok.Setter;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import ru.rencredit.volkov.reminder.AbstractWebTest;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@Setter
public class ScheduleRestEndpointTest extends AbstractWebTest {

    @Test
    @SneakyThrows
    public void getAllSchedulesTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/schedules"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray());
    }

}