package ru.rencredit.volkov.reminder.controller.view;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.rencredit.volkov.reminder.AbstractWebTest;
import ru.rencredit.volkov.reminder.util.EmailBuilderUtil;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@Setter
public class DestinationControllerTest extends AbstractWebTest {

    @Test
    @SneakyThrows
    public void destinationsTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("index"));
    }

    @Test
    @SneakyThrows
    public void createDestinationGetTest() {
        super.mockMvc.perform(MockMvcRequestBuilders
                .get("/destination/subscribe"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(view().name("destination-insert"));
    }

    @Test
    @SneakyThrows
    public void createDestinationPostTest() {
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post("/destination/subscribe")
                .flashAttr("email", email))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

    @Test
    @SneakyThrows
    public void deleteDestinationTest() {
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        this.mockMvc.perform(MockMvcRequestBuilders
                .get("/destination/unsubscribe/{email}", email))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }

}