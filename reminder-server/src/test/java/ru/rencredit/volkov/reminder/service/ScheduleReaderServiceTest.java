package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.ArrayUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.ISchedulerReaderService;
import ru.rencredit.volkov.reminder.casedata.StringCaseDataProvider;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.util.Collection;

@Setter
public class ScheduleReaderServiceTest extends AbstractTest {

    @Autowired
    private ISchedulerReaderService schedulerReaderService;

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void negativeReadFromResourcesTest() {
        Assertions.assertNotNull(this.schedulerReaderService);
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(invalidPathLine ->
                Assertions.assertThrows(InvalidParamException.class,
                        () -> this.schedulerReaderService.readFromResources(invalidPathLine)));
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void readFromResourcesTest() {
        Assertions.assertNotNull(this.schedulerReaderService);
        @NotNull final Collection<RestSchedulePOJO> schedules = this.schedulerReaderService.readFromResources();
        Assertions.assertNotNull(schedules);
        Assertions.assertNotEquals(0, schedules.size());
        schedules.forEach(schedule -> {
            Assertions.assertFalse(ArrayUtils.isEmpty(schedule.getJanuaryDates()));
            Assertions.assertFalse(ArrayUtils.isEmpty(schedule.getDecemberDates()));
        });
    }

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void readFromResourcesByPathnameTest() {
        Assertions.assertNotNull(this.schedulerReaderService);
        @NotNull final Collection<RestSchedulePOJO> schedules = this.schedulerReaderService.readFromResources("schedule.csv");
        Assertions.assertNotNull(schedules);
        Assertions.assertNotEquals(0, schedules.size());
        schedules.forEach(schedule -> {
            Assertions.assertFalse(ArrayUtils.isEmpty(schedule.getJanuaryDates()));
            Assertions.assertFalse(ArrayUtils.isEmpty(schedule.getDecemberDates()));
        });
    }

}