package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.casedata.StringCaseDataProvider;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.domain.entity.Destination;
import ru.rencredit.volkov.reminder.util.EmailBuilderUtil;

import java.util.Collection;
import java.util.UUID;

@Setter
public class DestinationServiceTest extends AbstractTest {


    @Autowired
    private IDestinationService destinationService;

    @Test
    @IntegrationImplementation
    public void subscribeNegativeTest() {
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(invalidStringLine ->
                Assertions.assertThrows(InvalidParamException.class,
                        () -> this.destinationService.subscribe(invalidStringLine)));
    }

    @Test
    @IntegrationImplementation
    public void unsubscribeByEmailNegativeTest() {
        StringCaseDataProvider.INVALID_STRING_STREAM.get().forEach(invalidStringLine ->
                Assertions.assertThrows(InvalidParamException.class,
                        () -> this.destinationService.unsubscribe(invalidStringLine)));
    }

    @Test
    @IntegrationImplementation
    public void subscribeTest() throws InvalidParamException {
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        @NotNull final Destination subscriber = this.destinationService.subscribe(email);
        Assertions.assertNotNull(subscriber);
        Assertions.assertEquals(email, subscriber.getEmail());
    }

    @Test
    @IntegrationImplementation
    public void unsubscribeTest() throws InvalidParamException {
        @NotNull final String email = EmailBuilderUtil.buildFrom(UUID.randomUUID().toString());
        @NotNull final Destination subscriber = this.destinationService.subscribe(email);
        Assertions.assertNotNull(subscriber);
        final int unsubscribeFlag = this.destinationService.unsubscribe(email);
        Assertions.assertEquals(1, unsubscribeFlag);
    }

    @Test
    @IntegrationImplementation
    public void getAllDestinationMailsTest() {
        @NotNull final Collection<String> emails = this.destinationService.getAllDestinationsEmail();
        Assertions.assertNotNull(emails);
        Assertions.assertNotEquals(0, emails.size());
    }

    @Test
    @IntegrationImplementation
    public void getAllDestinationsTest() {
        @NotNull final Collection<Destination> destinations = this.destinationService.getAllDestinations();
        Assertions.assertNotNull(destinations);
        Assertions.assertNotEquals(0, destinations.size());
    }

}