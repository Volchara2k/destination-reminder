package ru.rencredit.volkov.reminder.service;

import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.rencredit.volkov.reminder.AbstractTest;
import ru.rencredit.volkov.reminder.api.service.IEmailSenderService;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.exception.InvalidSendEmailException;
import ru.rencredit.volkov.reminder.marker.IntegrationImplementation;
import ru.rencredit.volkov.reminder.util.EmailBuilderUtil;

import java.util.UUID;

@Setter
public class EmailSenderServiceTest extends AbstractTest {

    @Autowired
    private IEmailSenderService emailSenderService;

    @Test
    @IntegrationImplementation
    public void negativeSendToTest() {
        Assertions.assertNotNull(this.emailSenderService);
        Assertions.assertThrows(InvalidAddresseeException.class,
                () -> this.emailSenderService.sendTo(null)
        );
    }

    @Test
    @IntegrationImplementation
    public void negativeSendingMessageTest() {
        Assertions.assertThrows(InvalidSendEmailException.class,
                () -> this.emailSenderService.sendTo(
                        new String[]{UUID.randomUUID().toString()}
                )
        );
    }

    @NotNull
    private static final String[] SUBSCRIBERS_ARRAY = new String[]{
            EmailBuilderUtil.buildFrom(UUID.randomUUID().toString()),
            EmailBuilderUtil.buildFrom(UUID.randomUUID().toString()),
            EmailBuilderUtil.buildFrom(UUID.randomUUID().toString())
    };

    @Test
    @SneakyThrows
    @IntegrationImplementation
    public void sendToTest() {
        final boolean isSendTo = this.emailSenderService.sendTo(SUBSCRIBERS_ARRAY);
        Assertions.assertTrue(isSendTo);
    }

}