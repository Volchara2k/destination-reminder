package ru.rencredit.volkov.reminder.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.rencredit.volkov.reminder.exception.*;
import ru.rencredit.volkov.reminder.domain.dto.ApiExceptionResponseDTO;

import javax.validation.ConstraintViolationException;

@Slf4j
@ControllerAdvice("ru.rencredit.volkov.reminder.controller.view")
public class ExceptionControllerAdvice {

    @NotNull
    @Order(1)
    @ExceptionHandler({
            DuplicateKeyException.class,
            InvalidParamException.class,
            InvalidAddresseeException.class,
            ConstraintViolationException.class
    })
    public String handleBadRequest(
            @Nullable final Exception exception,
            @NotNull final Model model
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildModel(HttpStatus.BAD_REQUEST, exception, model);
    }

    @NotNull
    @Order(2)
    @ExceptionHandler({
            LockedException.class,
            DisabledException.class,
            BadCredentialsException.class
    })
    public String handleUnauthorized(
            @Nullable final Exception exception,
            @NotNull final Model model
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildModel(HttpStatus.UNAUTHORIZED, exception, model);
    }

    @NotNull
    @Order(3)
    @ExceptionHandler(AccessDeniedException.class)
    public String handleForbidden(
            @Nullable final Exception exception,
            @NotNull final Model model
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildModel(HttpStatus.FORBIDDEN, exception, model);
    }

    @NotNull
    @Order(4)
    @ExceptionHandler({
            InvalidScheduleYearException.class,
            NotFoundDestinationException.class
    })
    public String handleNotFound(
            @Nullable final Exception exception,
            @NotNull final Model model
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildModel(HttpStatus.NOT_FOUND, exception, model);
    }

    @NotNull
    @Order(5)
    @ExceptionHandler(Exception.class)
    public String handleInternalServer(
            @Nullable final Exception exception,
            @NotNull final Model model
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildModel(HttpStatus.INTERNAL_SERVER_ERROR, exception, model);
    }

    @NotNull
    public String buildModel(
            @NotNull final HttpStatus httpStatus,
            @NotNull final Exception exception,
            @NotNull final Model model
    ) {
        @NotNull final ApiExceptionResponseDTO apiResponse = new ApiExceptionResponseDTO(httpStatus, exception);
        model.addAttribute("apiExceptionResponse", apiResponse);
        return "error";
    }

}