package ru.rencredit.volkov.reminder.api.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

public interface IExceptionController {

    @NotNull
    @GetMapping("/login-error.html")
    ModelAndView loginException(@NotNull Model model);

}