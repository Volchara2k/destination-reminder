package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IEmailSenderService;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.exception.InvalidSendEmailException;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailSenderService implements IEmailSenderService {

    @NotNull
    private final JavaMailSender javaMailSender;

    @NotNull
    private final SimpleMailMessage simpleMailMessage;

    @Override
    public boolean sendTo(
            @Nullable final String[] to
    ) throws InvalidAddresseeException, InvalidSendEmailException {
        log.info("send to {} elements pending", to.length);
        if (ValidRuleUtil.isNullOrEmpty(to)) {
            log.error("send to error {}", InvalidAddresseeException.class);
            throw new InvalidAddresseeException();
        }

        log.info("send to {} elements process", to.length);
        this.simpleMailMessage.setTo(to);
        return this.sendingMessage();
    }

    private boolean sendingMessage(
    ) throws InvalidSendEmailException {
        try {
            log.info("sending message process");
            this.javaMailSender.send(
                    this.simpleMailMessage
            );
        } catch (@NotNull final Exception exception) {
            log.error("sending message error {}", InvalidSendEmailException.class);
            log.info(exception.getLocalizedMessage());
            throw new InvalidSendEmailException("Invalid sending message to email", exception);
        }

        log.info("sending message fine");
        return true;
    }

}