package ru.rencredit.volkov.reminder.domain.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.springframework.mail.SimpleMailMessage;

@Data
@EqualsAndHashCode(callSuper = true)
public final class EmailMessagePOJO extends SimpleMailMessage {

    @NotNull
    private static final String DEFAULT_MESSAGE = "Empty message";

    @SuppressWarnings("unused")
    public EmailMessagePOJO() {
        setText(DEFAULT_MESSAGE);
    }

    public EmailMessagePOJO(@NotNull final String text) {
        setText(text);
    }

}