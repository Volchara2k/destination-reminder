package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IAuthenticationService;
import ru.rencredit.volkov.reminder.api.service.IUserService;
import ru.rencredit.volkov.reminder.domain.dto.SecureUserDTO;
import ru.rencredit.volkov.reminder.domain.entity.User;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationService implements IAuthenticationService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final AuthenticationManager authenticationManager;

    @NotNull
    @Override
    public SecureUserDTO signIn(
            @Nullable final String login, @Nullable final String password
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) {
            throw new InvalidParamException("login");
        }
        if (ValidRuleUtil.isNullOrEmpty(password)) {
            throw new InvalidParamException("password");
        }
        return this.authenticationProcess(login, password);
    }

    @NotNull
    @Override
    public User signUp(
            @Nullable final String login, @Nullable final String password
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(login)) {
            throw new InvalidParamException("login");
        }
        if (ValidRuleUtil.isNullOrEmpty(password)) {
            throw new InvalidParamException("password");
        }
        return this.userService.createUser(login,password);
    }

    @NotNull
    private SecureUserDTO authenticationProcess(
            @NotNull final String login, @NotNull final String password
    ) {
        @NotNull final Authentication authentication = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login, password)
        );
        return this.fetchCurrentUser(authentication);
    }

    @NotNull
    private SecureUserDTO fetchCurrentUser(
            @NotNull final Authentication authentication
    ) {
        @NotNull final Object principal = authentication.getPrincipal();
        if (principal instanceof SecureUserDTO) return (SecureUserDTO) principal;
        throw new AccessDeniedException("Непредвиденная ошибка аутентификации!");
    }

}