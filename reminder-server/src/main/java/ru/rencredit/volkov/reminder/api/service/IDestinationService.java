package ru.rencredit.volkov.reminder.api.service;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

import javax.annotation.PostConstruct;
import java.util.Collection;

public interface IDestinationService {

    @NotNull
    Destination subscribe(@Nullable String email) throws InvalidParamException;

    int unsubscribe(@Nullable String email) throws InvalidParamException;

    @NonNull
    Collection<String> getAllDestinationsEmail();

    @Nullable
    Destination getDestinationById(String id) throws InvalidParamException;

    @NotNull
    Collection<Destination> getAllDestinations();

    @NotNull
    @SuppressWarnings("unused")
    @PostConstruct
    Collection<Destination> initialDemoData();

}