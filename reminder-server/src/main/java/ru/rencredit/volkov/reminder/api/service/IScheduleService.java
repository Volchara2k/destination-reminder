package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.io.IOException;
import java.util.Collection;

public interface IScheduleService {

    @NotNull
    RestSchedulePOJO getScheduleByYear(
            int year
    ) throws InvalidParamException, InvalidScheduleYearException, IOException;

    @NotNull
    Collection<RestSchedulePOJO> getAllSchedules(
    ) throws InvalidParamException, IOException;

}