package ru.rencredit.volkov.reminder.exception.handler;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.rencredit.volkov.reminder.exception.*;
import ru.rencredit.volkov.reminder.domain.dto.ApiExceptionResponseDTO;

import javax.validation.ConstraintViolationException;

@Slf4j
@RestControllerAdvice({
        "ru.rencredit.volkov.reminder.endpoint.rest",
        "ru.rencredit.volkov.reminder.security.jwt"
})
public class RestExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    @NotNull
    @Order(1)
    @ExceptionHandler({
            DuplicateKeyException.class,
            InvalidParamException.class,
            IllegalArgumentException.class,
            InvalidAddresseeException.class,
            ConstraintViolationException.class
    })
    public ResponseEntity<ApiExceptionResponseDTO> handleBadRequest(
            @NotNull final Exception exception
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildResponse(HttpStatus.BAD_REQUEST, exception);
    }

    @NotNull
    @Order(2)
    @ExceptionHandler({
            LockedException.class,
            DisabledException.class,
            BadCredentialsException.class
    })
    public ResponseEntity<ApiExceptionResponseDTO> handleUnauthorized(
            @NotNull final Exception exception
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildResponse(HttpStatus.UNAUTHORIZED, exception);
    }

    @NotNull
    @Order(3)
    @ExceptionHandler({
            AccessDeniedException.class,
            InvalidJwtTokenException.class
    })
    public ResponseEntity<ApiExceptionResponseDTO> handleForbidden(
            @NotNull final Exception exception
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildResponse(HttpStatus.FORBIDDEN, exception);
    }

    @NotNull
    @Order(4)
    @ExceptionHandler({
            NotFoundDestinationException.class,
            InvalidScheduleYearException.class
    })
    public ResponseEntity<ApiExceptionResponseDTO> handleNotFound(
            @NotNull final Exception exception
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildResponse(HttpStatus.NOT_FOUND, exception);
    }

    @NotNull
    @Order(5)
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiExceptionResponseDTO> defaultHandler(
            @NotNull final Exception exception
    ) {
        log.error("Exception {} during execution of application ", exception.getClass());
        return this.buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    @NotNull
    private ResponseEntity<ApiExceptionResponseDTO> buildResponse(
            @NotNull final HttpStatus httpStatus,
            @NotNull final Exception exception
    ) {
        @NotNull final ApiExceptionResponseDTO apiResponse = new ApiExceptionResponseDTO(httpStatus, exception);
        return new ResponseEntity<>(apiResponse, httpStatus);
    }

}