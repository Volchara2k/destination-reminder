package ru.rencredit.volkov.reminder.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.rencredit.volkov.reminder.domain.dto.AuthenticationRequestDTO;
import ru.rencredit.volkov.reminder.domain.dto.CreateUserRequestDTO;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;

import javax.validation.Valid;

public interface IAuthenticationRestEndpoint {

    @NotNull
    @PostMapping("/login")
    @ApiOperation(
            value = "Sign in process",
            notes = "User sign in process. Authentication request required."
    )
    ResponseEntity<Void> login(
            @ApiParam(
                    name = "authenticationRequest",
                    value = "Authentication user request",
                    required = true
            )
            @RequestBody @Valid AuthenticationRequestDTO authenticationRequest
    ) throws InvalidParamException;

    @NotNull
    @PostMapping("/sign-up")
    @ApiOperation(
            value = "Sign up process",
            notes = "User sign up process. Create user request required."
    )
    ResponseEntity<Void> signUp(
            @ApiParam(
                    name = "createUserRequest",
                    value = "Create user request",
                    required = true
            )
            @RequestBody @Valid @NotNull CreateUserRequestDTO createUserRequest
    ) throws InvalidParamException;

}