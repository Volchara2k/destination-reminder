package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.domain.dto.SecureUserDTO;
import ru.rencredit.volkov.reminder.domain.entity.User;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;

public interface IAuthenticationService {

    @NotNull SecureUserDTO signIn(
            @Nullable String login, @Nullable String password
    ) throws InvalidParamException;

    @NotNull User signUp(
            @Nullable String login, @Nullable String password
    ) throws InvalidParamException;

}