package ru.rencredit.volkov.reminder.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rencredit.volkov.reminder.api.endpoint.rest.IScheduleRestEndpoint;
import ru.rencredit.volkov.reminder.api.service.IScheduleService;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.util.Collection;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public final class ScheduleRestEndpoint implements IScheduleRestEndpoint {

    @NotNull
    private final IScheduleService scheduleService;

    @NotNull
    @SneakyThrows
    @GetMapping("/schedules")
    @ApiOperation(
            value = "Get all schedules",
            notes = "Returns a complete list of schedules details."
    )
    @Override
    public ResponseEntity<Collection<RestSchedulePOJO>> getAllSchedules() {
        log.info("get all schedules");
        return ResponseEntity.ok(
                this.scheduleService.getAllSchedules()
        );
    }

}