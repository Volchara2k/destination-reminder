package ru.rencredit.volkov.reminder.domain.dto;

import lombok.Data;
import org.jetbrains.annotations.Nullable;

@Data
public final class AuthenticationRequestDTO {

    @Nullable
    private String login;

    @Nullable
    private String password;

}