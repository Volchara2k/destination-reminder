package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rencredit.volkov.reminder.api.service.IUserReaderService;
import ru.rencredit.volkov.reminder.api.service.IUserService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.entity.User;
import ru.rencredit.volkov.reminder.domain.pojo.UserPOJO;
import ru.rencredit.volkov.reminder.repository.IUserRepository;
import ru.rencredit.volkov.reminder.util.AdapterUserUtil;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(
        rollbackFor = Exception.class
)
public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IUserReaderService userReaderService;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    @NotNull
    @Override
    public User createUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws InvalidParamException {
        log.info("add user {} process", login);
        if (ValidRuleUtil.isNullOrEmpty(login)) {
            log.error("add user error {}", InvalidParamException.class);
            throw new InvalidParamException("логин");
        }
        if (ValidRuleUtil.isNullOrEmpty(password)) {
            log.error("add user error {}", InvalidParamException.class);
            throw new InvalidParamException("пароль");
        }

        @NotNull final String passwordHash = this.passwordEncoder.encode(password);
        @NotNull final User user = new User(login, passwordHash);
        return this.userRepository.insert(user);
    }


    @Nullable
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public User getUserByLogin(
            @Nullable final String login
    ) throws InvalidParamException {
        log.info("get by login {} pending", login);
        if (ValidRuleUtil.isNullOrEmpty(login)) {
            log.error("get by login error {}", InvalidParamException.class);
            throw new InvalidParamException("логин");
        }

        log.info("get by login {} process", login);
        return this.userRepository.findByLogin(login);
    }

    @NotNull
    @PostConstruct
    @Override
    public Collection<User> initialDemoData() throws InvalidParamException, IOException {
        log.info("initial demo data pending");
        final long existDataCount = this.userRepository.count();
        final boolean isNotExistData = existDataCount == 0;

        if (isNotExistData) {
            @NotNull final Collection<UserPOJO> usersPOJO = this.userReaderService.readFromResources();
            log.info("initial demo data process {}", usersPOJO);
            @NotNull final Collection<User> users = AdapterUserUtil.forUsers.apply(usersPOJO);
            log.info("initial demo data fine {}", usersPOJO);
            return this.userRepository.insert(users);
        }

        return Collections.emptyList();
    }

}