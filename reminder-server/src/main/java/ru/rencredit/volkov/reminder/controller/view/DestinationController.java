package ru.rencredit.volkov.reminder.controller.view;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.volkov.reminder.annotation.Admin;
import ru.rencredit.volkov.reminder.annotation.Auth;
import ru.rencredit.volkov.reminder.api.controller.view.IDestinationController;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.api.service.IEmailNotificatorService;
import ru.rencredit.volkov.reminder.domain.entity.Destination;
import ru.rencredit.volkov.reminder.util.RedirectUtil;

import java.util.Collection;

@Slf4j
@Controller
@RequiredArgsConstructor
public class DestinationController implements IDestinationController {

    @NotNull
    private final IDestinationService destinationService;

    @NotNull
    private final IEmailNotificatorService emailNotificatorService;

    @Auth
    @NotNull
    @GetMapping("/destinations")
    @Override
    public ModelAndView subscribersList() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("destination-list");
        @NotNull final Collection<Destination> destinations = this.destinationService.getAllDestinations();
        log.info("subscribers list {}", destinations);
        modelAndView.addObject("destinations", destinations);
        return modelAndView;
    }

    @Auth
    @NotNull
    @GetMapping("/destination/subscribe")
    @Override
    public ModelAndView subscribe() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("destination-insert");
        log.info("subscribe process");
        modelAndView.addObject("email", "");
        return modelAndView;
    }

    @Auth
    @NotNull
    @PostMapping("/destination/subscribe")
    @SneakyThrows
    @Override
    public ModelAndView subscribe(
            @ModelAttribute("email") @NotNull final String email,
            @NotNull final BindingResult result
    ) {
        log.info("subscribe {}", email);
        this.destinationService.subscribe(email);
        return new ModelAndView(RedirectUtil.redirect("destinations"));
    }

    @Auth
    @NotNull
    @GetMapping("/destination/unsubscribe/{email}")
    @SneakyThrows
    @Override
    public ModelAndView unsubscribe(
            @PathVariable("email") @NotNull final String email
    ) {
        log.info("unsubscribe {}", email);
        this.destinationService.unsubscribe(email);
        return new ModelAndView(RedirectUtil.redirect("destinations"));
    }

    @Admin
    @NotNull
    @GetMapping("/destinations/remind")
    @SneakyThrows
    @Override
    public ModelAndView remind() {
        log.info("remind");
        this.emailNotificatorService.notifySubscribers();
        return new ModelAndView(RedirectUtil.redirect());
    }

}