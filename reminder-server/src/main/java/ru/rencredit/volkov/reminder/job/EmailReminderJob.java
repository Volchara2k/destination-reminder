package ru.rencredit.volkov.reminder.job;

import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import ru.rencredit.volkov.reminder.api.service.IEmailNotificatorService;
import ru.rencredit.volkov.reminder.api.service.IScheduleListenerService;

@Slf4j
@Setter
@Component
@SuppressWarnings("SpringJavaAutowiredFieldsWarningInspection")
public class EmailReminderJob implements Job {

    @NotNull
    private static final String EXCEPTION_REMIND =
            "The emails sending process must be positive!\n" +
                    "Check out the postfix for your email address " +
                    "and the presence of addressees!";

    @NotNull
    @Autowired
    private IEmailNotificatorService emailNotificatorService;

    @NotNull
    @Autowired
    private IScheduleListenerService scheduleListenerService;

    @SneakyThrows
    @Override
    public void execute(@NotNull final JobExecutionContext jobExecutionContext) {
        log.info("execute pending");
        final boolean todayNotDispatchDay = !this.scheduleListenerService.todayIsDispatchDay();
        if (todayNotDispatchDay) return;
        log.info("execute process");
        final boolean isRemind = this.emailNotificatorService.notifySubscribers();
        Assert.isTrue(isRemind, EXCEPTION_REMIND);
        log.info("execute fine");
    }

}