package ru.rencredit.volkov.reminder.api.service;

import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

public interface ISchedulerReaderService extends ICsvReaderService<RestSchedulePOJO> {
}