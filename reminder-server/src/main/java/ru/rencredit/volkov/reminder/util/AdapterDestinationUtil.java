package ru.rencredit.volkov.reminder.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import ru.rencredit.volkov.reminder.domain.entity.Destination;
import ru.rencredit.volkov.reminder.domain.pojo.DestinationPOJO;

import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Function;

@Slf4j
@UtilityClass

public class AdapterDestinationUtil {

    @NotNull
    public Function<Collection<DestinationPOJO>, Collection<Destination>> forDestinations = destinationsPOJO -> {
        @NotNull final Collection<Destination> destinations = new LinkedList<>();
        log.info("conversion process {} pending", destinationsPOJO);
        destinationsPOJO.forEach(destinationPOJO -> destinations.add(
                Destination.builder()
                        .id(destinationPOJO.getId())
                        .email(destinationPOJO.getEmail())
                        .build()
        ));
        log.info("conversion process {} fine", destinations);
        return destinations;
    };

}