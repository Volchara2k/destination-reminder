package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addViewControllers(@NotNull final ViewControllerRegistry viewControllerRegistry) {
        viewControllerRegistry
                .addViewController("/")
                .setViewName("/index");
        viewControllerRegistry
                .addViewController("/login")
                .setViewName("/login");
    }

}