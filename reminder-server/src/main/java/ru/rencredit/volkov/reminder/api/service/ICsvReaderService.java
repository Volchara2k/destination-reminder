package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;

import java.io.IOException;
import java.util.Collection;

public interface ICsvReaderService<T> {

    @NotNull
    Collection<T> readFromResources(
    ) throws InvalidParamException, IOException;

    @NotNull
    Collection<T> readFromResources(
            @Nullable String pathname
    ) throws InvalidParamException, IOException;

}