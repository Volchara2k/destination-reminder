package ru.rencredit.volkov.reminder.exception;

public final class InvalidAddresseeException extends AbstractException {

    public InvalidAddresseeException() {
        super("Обнаружены недопустимые адресанты");
    }

}