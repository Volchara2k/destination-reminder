package ru.rencredit.volkov.reminder.util;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;

@Slf4j
@UtilityClass
@SuppressWarnings("unused")
public class CvsUtil {

    @NotNull
    public <T> Collection<T> parseFromFor(
            @NotNull final File from, @NotNull final Class<T> forClass
    ) throws IOException {
        log.info("parse from {} for {} pending", from.getAbsolutePath(), forClass);
        try (@NotNull final FileReader fileReader = new FileReader(from)) {
            log.info("parse from {} for {} process", from.getAbsolutePath(), forClass);
            return cvsToBean(fileReader, forClass)
                    .parse();
        } catch (@NotNull final IOException exception) {
            log.error("parse from for error {}", IOException.class);
            log.info(exception.getLocalizedMessage());
            throw new IOException("Invalid parsing exception");
        }
    }

    @NotNull
    public <T> CsvToBean<T> cvsToBean(@NotNull final InputStreamReader reader, Class<T> to) {
        log.info("cvs to bean {}", to);
        return new CsvToBeanBuilder<T>(reader)
                .withType(to)
                .withIgnoreLeadingWhiteSpace(true)
                .build();
    }

}