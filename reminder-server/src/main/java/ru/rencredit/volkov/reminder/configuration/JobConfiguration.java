package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rencredit.volkov.reminder.job.EmailReminderJob;

import javax.annotation.PostConstruct;

@Configuration
public class JobConfiguration {

    @NotNull
    private final Scheduler scheduler;

    @NotNull
    private final String reminderCron;

    @NotNull
    private final String reminderGroup;

    @NotNull
    private final String reminderJob;

    @NotNull
    private final String reminderTrigger;

    public JobConfiguration(
            @NotNull final Scheduler scheduler,
            @Value("${schedule.reminder.cron}") @NotNull final String reminderCron,
            @Value("${schedule.reminder.group}") @NotNull final String reminderGroup,
            @Value("${schedule.reminder.job}") @NotNull final String reminderJob,
            @Value("${schedule.reminder.trigger}") @NotNull final String reminderTrigger
    ) {
        this.scheduler = scheduler;
        this.reminderCron = reminderCron;
        this.reminderGroup = reminderGroup;
        this.reminderJob = reminderJob;
        this.reminderTrigger = reminderTrigger;
    }

    @PostConstruct
    public void initialize() throws SchedulerException {
        this.scheduler.addJob(jobDetail(), true, true);
        final boolean isExistTriggerOfGroup = this.isExistTrigger(this.reminderTrigger, this.reminderGroup);
        if (isExistTriggerOfGroup) return;
        this.scheduler.scheduleJob(cronTrigger());
    }

    @Bean
    @NotNull
    public JobDetail jobDetail() {
        return JobBuilder.newJob(EmailReminderJob.class)
                .storeDurably()
                .withIdentity(this.reminderJob, this.reminderGroup)
                .build();
    }

    @Bean
    @NotNull
    public CronTrigger cronTrigger() {
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail())
                .withIdentity(this.reminderTrigger, this.reminderGroup)
                .withSchedule(
                        CronScheduleBuilder.
                                cronSchedule(this.reminderCron)
                )
                .build();
    }

    private boolean isExistTrigger(
            @NotNull final String trigger,
            @NotNull final String group
    ) throws SchedulerException {
        return this.scheduler.checkExists(new TriggerKey(trigger, group));
    }

}