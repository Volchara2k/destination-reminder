package ru.rencredit.volkov.reminder.service;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.rencredit.volkov.reminder.api.service.IDestinationReaderService;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.entity.Destination;
import ru.rencredit.volkov.reminder.domain.pojo.DestinationPOJO;
import ru.rencredit.volkov.reminder.repository.IDestinationRepository;
import ru.rencredit.volkov.reminder.util.AdapterDestinationUtil;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(
        rollbackFor = Exception.class
)
public class DestinationService implements IDestinationService {

    @NonNull
    private final IDestinationRepository destinationRepository;

    @NotNull
    private final IDestinationReaderService destinationReaderService;

    @NotNull
    @Override
    public Destination subscribe(@Nullable final String email) throws InvalidParamException {
        log.info("subscribe {} pending", email);
        if (ValidRuleUtil.isNullOrEmpty(email)) {
            log.error("subscribe error {}", InvalidParamException.class);
            throw new InvalidParamException("email");
        }

        log.info("subscribe {} process", email);
        @NotNull final Destination destination = new Destination(email);
        return this.destinationRepository.insert(destination);
    }

    @Override
    public int unsubscribe(@Nullable final String email) throws InvalidParamException {
        log.info("unsubscribe {} pending", email);
        if (ValidRuleUtil.isNullOrEmpty(email)) {
            log.error("unsubscribe error {}", InvalidParamException.class);
            throw new InvalidParamException("email");
        }

        log.info("unsubscribe {} process", email);
        return this.destinationRepository.deleteByEmail(email);
    }

    @NonNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public Collection<String> getAllDestinationsEmail() {
        log.info("get all destination emails");
        return this.getAllDestinations().stream()
                .map(Destination::getEmail)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Destination getDestinationById(@Nullable final String id) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return this.destinationRepository.findById(id)
                .orElse(null);
    }

    @NotNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public Collection<Destination> getAllDestinations() {
        log.info("get all destinations");
        return new ArrayList<>(this.destinationRepository.findAll());
    }

    @NotNull
    @SneakyThrows
    @PostConstruct
    @Override
    public Collection<Destination> initialDemoData() {
        log.info("initial demo data pending");
        final long existDataCount = this.destinationRepository.count();
        final boolean isNotExistData = existDataCount == 0;

        if (isNotExistData) {
            @NotNull final Collection<DestinationPOJO> destinationsPOJO = this.destinationReaderService.readFromResources();
            log.info("initial demo data process {}", destinationsPOJO);
            @NotNull final Collection<Destination> destinations = AdapterDestinationUtil.forDestinations.apply(destinationsPOJO);
            log.info("initial demo data fine {}", destinationsPOJO);
            return this.destinationRepository.insert(destinations);
        }

        return Collections.emptyList();
    }

}