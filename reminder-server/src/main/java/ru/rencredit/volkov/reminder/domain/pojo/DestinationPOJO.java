package ru.rencredit.volkov.reminder.domain.pojo;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class DestinationPOJO {

    @NotNull
    @CsvBindByName(column = "Идентификатор")
    private String id;

    @NotNull
    @CsvBindByName(column = "Почта")
    private String email = "adm_vvolkov@rencredit.ru";

}