package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IScheduleService;
import ru.rencredit.volkov.reminder.api.service.ISchedulerReaderService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.io.IOException;
import java.util.Collection;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduleService implements IScheduleService {

    @NotNull
    private final ISchedulerReaderService schedulerReaderService;

    @NotNull
    @Cacheable(value = "schedule", key = "#year")
    @Override
    public RestSchedulePOJO getScheduleByYear(
            final int year
    ) throws InvalidParamException, InvalidScheduleYearException, IOException {
        log.info("get schedule by year {}", year);
        return this.getAllSchedules().stream()
                .filter(schedule -> year == schedule.getYear())
                .findFirst()
                .orElseThrow(() -> {
                    log.error("get schedule by year error {}", InvalidScheduleYearException.class);
                    return new InvalidScheduleYearException();
                });
    }

    @NotNull
    @Cacheable(value = "schedules")
    @Override
    public Collection<RestSchedulePOJO> getAllSchedules(
    ) throws InvalidParamException, IOException {
        log.info("get all schedules");
        return this.schedulerReaderService.readFromResources();
    }

}