package ru.rencredit.volkov.reminder.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rencredit.volkov.reminder.domain.entity.User;

public interface IUserRepository extends MongoRepository<User, String> {

    @Nullable
    User findByLogin(@NotNull String login);

}