package ru.rencredit.volkov.reminder.exception;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class InvalidJwtTokenException extends AbstractException {

    public InvalidJwtTokenException(@Nullable final String message, @NotNull final Throwable throwable) {
        super(message, throwable);
    }

}