package ru.rencredit.volkov.reminder.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.NotFoundDestinationException;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

import java.util.Collection;

@SuppressWarnings("unused")
public interface IDestinationRestEndpoint {

    @NotNull
    @GetMapping("/destinations")
    @ApiOperation(
            value = "Get all destinations",
            notes = "Returns a complete list of destination details."
    )
    ResponseEntity<Collection<Destination>> getAllDestination();

    @NotNull
    @GetMapping("/destinations/emails")
    @ApiOperation(
            value = "Get all destination emails",
            notes = "Returns a complete list of destination emails details."
    )
    ResponseEntity<Collection<String>> getAllDestinationsEmail();

    @NotNull
    @GetMapping(
            value = "/destination/{id}"
    )
    @ApiOperation(
            value = "Get destination by unique ID",
            notes = "Return destination by unique UD. Unique ID required."
    )
    ResponseEntity<Destination> getDestinationById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of destination",
                    example = "432roijr32iojr32rio3j",
                    required = true
            )
            @PathVariable("id") @NotNull String id
    ) throws InvalidParamException, NotFoundDestinationException;

    @NotNull
    @PostMapping(
            value = "/destination/subscribe"
    )
    @ApiOperation(
            value = "Subscribe destination",
            notes = "Returns subscribed destination. Created destination required."
    )
    ResponseEntity<Destination> subscribeDestination(
            @ApiParam(
                    name = "email",
                    value = "Unique email for destination",
                    example = "example@rencredit.ru",
                    required = true
            )
            @RequestBody @NotNull String email
    ) throws InvalidParamException;

    @NotNull
    @DeleteMapping("/destination/unsubscribe/{email}")
    @ApiOperation(
            value = "Unsubscribe destination by email",
            notes = "Returns integer unsubscribed flag: 1 - true, 0 - false. Unique email required."
    )
    ResponseEntity<Integer> unsubscribeDestination(
            @ApiParam(
                    name = "email",
                    value = "Unique email for destination",
                    example = "example@rencredit.ru",
                    required = true
            )
            @PathVariable("email") @NotNull String email
    ) throws InvalidParamException;

}