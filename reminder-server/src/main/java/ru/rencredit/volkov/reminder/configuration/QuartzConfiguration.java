package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import ru.rencredit.volkov.reminder.factory.job.AutowiringSpringBeanJobFactory;

@Configuration
public class QuartzConfiguration {

    @NotNull
    private static final String QUARTS_CONFIG_PATHNAME = "quartz.yml";

    @NotNull
    private final ApplicationContext applicationContext;

    public QuartzConfiguration(
            @NotNull final ApplicationContext applicationContext
    ) {
        this.applicationContext = applicationContext;
    }

    @Bean
    @NotNull
    public SpringBeanJobFactory springBeanJobFactory() {
        @NotNull final AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(this.applicationContext);
        return jobFactory;
    }

    @Bean
    @NotNull
    public SchedulerFactoryBean schedulerFactoryBean(
            @NotNull final SpringBeanJobFactory springBeanJobFactory
    ) {
        @NotNull final SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
        schedulerFactory.setConfigLocation(new ClassPathResource(QUARTS_CONFIG_PATHNAME));
        schedulerFactory.setWaitForJobsToCompleteOnShutdown(true);
        schedulerFactory.setJobFactory(springBeanJobFactory);
        return schedulerFactory;
    }

}