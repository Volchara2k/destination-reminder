package ru.rencredit.volkov.reminder.exception;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class InvalidLoadScheduleException extends AbstractException {

    public InvalidLoadScheduleException(@Nullable final String message, @NotNull final Throwable throwable) {
        super(message, throwable);
    }

}