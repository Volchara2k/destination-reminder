package ru.rencredit.volkov.reminder.api.service;

import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;

import java.io.IOException;

public interface IScheduleListenerService {

    boolean todayIsDispatchDay(
    ) throws InvalidParamException, InvalidScheduleYearException, IOException;

    boolean todayIsDispatchDay(
            int dayOfMonth, int monthValue, int year
    ) throws InvalidParamException, InvalidScheduleYearException, IOException;

}