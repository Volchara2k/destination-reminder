package ru.rencredit.volkov.reminder.security.jwt;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.rencredit.volkov.reminder.api.service.IUserService;
import ru.rencredit.volkov.reminder.domain.entity.User;
import ru.rencredit.volkov.reminder.util.AdapterUserUtil;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.function.Function;

import static java.util.Optional.ofNullable;

@Component
@RequiredArgsConstructor
public class JwtTokenFilter extends OncePerRequestFilter {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final JwtTokenUtil jwtTokenUtil;

    @SneakyThrows
    @Override
    protected void doFilterInternal(
            @Nullable final HttpServletRequest request,
            @Nullable final HttpServletResponse response,
            @Nullable final FilterChain chain
    ) {
        @Nullable final String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (this.isIncorrectHeader(header)) {
            chain.doFilter(request, response);
            return;
        }

        @Nullable final String jwtToken = this.extractJwtToken(header);
        if (!this.jwtTokenUtil.validate(jwtToken)) {
            chain.doFilter(request, response);
            return;
        }

        @NotNull final String login = this.jwtTokenUtil.parseLogin(jwtToken);
        @Nullable final User user = this.userService.getUserByLogin(login);
        @NotNull final UserDetails userDetails = AdapterUserUtil.forUserDetail.apply(user);
        @NotNull final UsernamePasswordAuthenticationToken authentication = this.authProcess.apply(userDetails);
        this.authenticationDetails(authentication, request);
        this.authentication(authentication);
        chain.doFilter(request, response);
    }

    private boolean isIncorrectHeader(
            @Nullable final String header
    ) {
        return ValidRuleUtil.isNullOrEmpty(header) || this.isInvalidHeader(header);
    }

    private boolean isInvalidHeader(
            @Nullable final String header
    ) {
        return !header.startsWith("Bearer ");
    }

    @Nullable
    private String extractJwtToken(
            @Nullable final String header
    ) {
        return header.split(" ")[1].trim();
    }

    @NotNull
    private final Function<UserDetails, UsernamePasswordAuthenticationToken> authProcess =
            details -> new UsernamePasswordAuthenticationToken(
                    details,
                    null,
                    ofNullable(details)
                            .map(UserDetails::getAuthorities)
                            .orElse(Collections.emptyList())
            );

    private void authenticationDetails(
            @NotNull final UsernamePasswordAuthenticationToken authentication,
            @NotNull final HttpServletRequest request
    ) {
        @NotNull final WebAuthenticationDetailsSource detailsSource = new WebAuthenticationDetailsSource();
        authentication.setDetails(
                detailsSource.buildDetails(request)
        );
    }

    private void authentication(
            @NotNull final UsernamePasswordAuthenticationToken authentication
    ) {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);
    }

}