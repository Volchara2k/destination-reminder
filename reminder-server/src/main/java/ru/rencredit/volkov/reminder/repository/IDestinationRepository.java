package ru.rencredit.volkov.reminder.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.repository.MongoRepository;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

public interface IDestinationRepository extends MongoRepository<Destination, String> {

    int deleteByEmail(@NotNull String email);

}