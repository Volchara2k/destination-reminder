package ru.rencredit.volkov.reminder.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class ExceptionController {

    @GetMapping("/login-error")
    public String loginError(@NotNull final Model model) {
        model.addAttribute("loginError", true);
        return "login.html";
    }

}