package ru.rencredit.volkov.reminder.security.jwt;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.rencredit.volkov.reminder.domain.dto.SecureUserDTO;
import ru.rencredit.volkov.reminder.exception.InvalidJwtTokenException;

import java.util.Date;

@Slf4j
@Component
public class JwtTokenUtil {

    @NotNull
    private final String jwtSecret;

    @NotNull
    private final String jwtIssuer;

    public JwtTokenUtil(
            @Value("${jwt.secret.key}") @NotNull final String jwtSecret,
            @Value("${jwt.secret.issuer}") @NotNull final String jwtIssuer
    ) {
        this.jwtSecret = jwtSecret;
        this.jwtIssuer = jwtIssuer;
    }

    @NotNull
    public String generateAccessToken(@NotNull final SecureUserDTO secureUserDTO) {
        return Jwts.builder()
                .setSubject(String.format("%s,%s", secureUserDTO.getId(), secureUserDTO.getUsername()))
                .setIssuer(this.jwtIssuer)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + 7 * 24 * 60 * 60 * 1000)) // 1 week
                .signWith(SignatureAlgorithm.HS512, this.jwtSecret)
                .compact();
    }

    @NotNull
    public String parseLogin(@NotNull final String jwtToken) {
        @NotNull final Claims claims = Jwts.parser()
                .setSigningKey(this.jwtSecret)
                .parseClaimsJws(jwtToken)
                .getBody();
        return claims.getSubject().split(",")[1];
    }

    public boolean validate(@Nullable final String jwtToken) throws InvalidJwtTokenException {
        try {
            Jwts.parser()
                    .setSigningKey(this.jwtSecret)
                    .parseClaimsJws(jwtToken);
            return true;
        } catch (@NotNull final SignatureException exception) {
            log.error("Invalid JWT signature - {}", exception.getMessage());
            throw new InvalidJwtTokenException("Invalid JWT signature", exception);
        } catch (@NotNull final MalformedJwtException exception) {
            log.error("Invalid JWT token - {}", exception.getMessage());
            throw new InvalidJwtTokenException("Invalid JWT token", exception);
        } catch (@NotNull final ExpiredJwtException exception) {
            log.error("Expired JWT token {}", exception.getMessage());
            throw new InvalidJwtTokenException("Expired JWT token", exception);
        } catch (@NotNull final UnsupportedJwtException exception) {
            log.error("Unsupported JWT token - {}", exception.getMessage());
            throw new InvalidJwtTokenException("Unsupported JWT token", exception);
        } catch (@NotNull final IllegalArgumentException exception) {
            log.error("JWT claims string is empty - {}", exception.getMessage());
            throw new InvalidJwtTokenException("JWT claims string is empty", exception);
        }
    }

}
