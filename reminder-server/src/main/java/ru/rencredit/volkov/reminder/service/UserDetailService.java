package ru.rencredit.volkov.reminder.service;

import com.netflix.discovery.util.StringUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IUserService;
import ru.rencredit.volkov.reminder.domain.entity.User;
import ru.rencredit.volkov.reminder.util.AdapterUserUtil;

import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserDetailService implements UserDetailsService {

    @NotNull
    private final IUserService userService;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(
            @Nullable final String login
    ) throws UsernameNotFoundException {
        log.info("load user by username {} pending", login);
        @Nullable final User user = this.userService.getUserByLogin(login);
        if (Objects.isNull(user)) {
            log.error("load user by username error {}", UsernameNotFoundException.class);
            throw new UsernameNotFoundException(
                    StringUtil.join("Не найден пользователь для: ", login)
            );
        }

        log.info("load user by username {} process", login);
        return AdapterUserUtil.forUserDetail.apply(user);
    }

}