package ru.rencredit.volkov.reminder.api.service;

import ru.rencredit.volkov.reminder.domain.pojo.DestinationPOJO;

public interface IDestinationReaderService extends ICsvReaderService<DestinationPOJO> {
}