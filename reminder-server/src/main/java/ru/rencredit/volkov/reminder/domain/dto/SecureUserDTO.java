package ru.rencredit.volkov.reminder.domain.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

@Getter
@Setter
public final class SecureUserDTO extends User {

    @NotNull
    private String id;

    @Builder(
            builderMethodName = "detailBuilder"
    )
    public SecureUserDTO(
            @NotNull final UserDetails userDetails,
            @NotNull final String id
    ) {
        super(
                userDetails.getUsername(), userDetails.getPassword(),
                userDetails.isEnabled(), userDetails.isAccountNonExpired(),
                userDetails.isCredentialsNonExpired(), userDetails.isAccountNonLocked(),
                userDetails.getAuthorities()
        );
        this.id = id;
    }

}