package ru.rencredit.volkov.reminder.configuration;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import ru.rencredit.volkov.reminder.security.jwt.JwtTokenFilter;

import javax.servlet.http.HttpServletResponse;

@Configuration
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @NotNull
    private final UserDetailsService userDetailsService;

    @NotNull
    private final JwtTokenFilter jwtTokenFilter;

    @NotNull
    private final PasswordEncoder passwordEncoder;

    @Bean
    @NotNull
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(
            @NotNull final AuthenticationManagerBuilder authenticationManagerBuilder
    ) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(this.passwordEncoder);
    }

    @Override
    protected void configure(
            @NotNull final HttpSecurity httpSecurity
    ) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers("/", "/login", "/login-error", "/error").permitAll()
                .antMatchers("/css/**", "/js/**", "/webjars/**").permitAll()
                .antMatchers("/v2/api-docs", "/swagger-ui.html").permitAll()
                .antMatchers("/schedules", "/api/v1/login*", "/api/v1/sign-up*").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/auth")
                .defaultSuccessUrl("/", true)
                .failureUrl("/login-error")
                .and()
                .logout().permitAll()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/")
                .deleteCookies("JSESSIONID")
                .invalidateHttpSession(true)
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(
                        (request, response, exception) -> response.sendError(
                                HttpServletResponse.SC_UNAUTHORIZED,
                                exception.getMessage()
                        )
                )
                .and()
                .addFilterBefore(
                        this.jwtTokenFilter,
                        UsernamePasswordAuthenticationFilter.class
                )
                .csrf().disable();
    }

}