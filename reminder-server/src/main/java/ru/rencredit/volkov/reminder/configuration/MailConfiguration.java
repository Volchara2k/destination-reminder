package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.SimpleMailMessage;
import ru.rencredit.volkov.reminder.domain.pojo.EmailMessagePOJO;

@Configuration
public class MailConfiguration {

    @NotNull
    private final String message;

    public MailConfiguration(
            @Value("${email.default-message}") @NotNull final String message
    ) {
        this.message = message;
    }

    @Bean
    @NotNull
    public SimpleMailMessage simpleMailMessage() {
        return new EmailMessagePOJO(this.message);
    }

}