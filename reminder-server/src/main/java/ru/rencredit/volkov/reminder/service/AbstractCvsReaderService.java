package ru.rencredit.volkov.reminder.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.api.service.ICsvReaderService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.util.CvsUtil;
import ru.rencredit.volkov.reminder.util.ResourceUtil;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import java.io.IOException;
import java.util.Collection;

@Slf4j
public abstract class AbstractCvsReaderService<T> implements ICsvReaderService<T> {

    @NotNull
    private final Class<T> parsableClass;

    protected AbstractCvsReaderService(
            @NotNull final Class<T> parsableClass
    ) {
        this.parsableClass = parsableClass;
    }

    @NotNull
    @Override
    public Collection<T> readFromResources(
            @Nullable final String pathname
    ) throws InvalidParamException, IOException {
        log.info("read from resources {} pending", pathname);
        if (ValidRuleUtil.isNullOrEmpty(pathname)) {
            log.error("read from resources error {}", InvalidParamException.class);
            throw new InvalidParamException("pathname");
        }

        log.info("read from resources {} process", pathname);
        return CvsUtil.parseFromFor(
                ResourceUtil.sourceFileFrom(pathname),
                this.parsableClass
        );
    }

}