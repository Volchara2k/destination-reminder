package ru.rencredit.volkov.reminder.endpoint.rest.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.rencredit.volkov.reminder.api.endpoint.rest.IAuthenticationRestEndpoint;
import ru.rencredit.volkov.reminder.domain.dto.AuthenticationRequestDTO;
import ru.rencredit.volkov.reminder.domain.dto.CreateUserRequestDTO;
import ru.rencredit.volkov.reminder.domain.dto.SecureUserDTO;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.service.AuthenticationService;
import ru.rencredit.volkov.reminder.security.jwt.JwtTokenUtil;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/v1",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public final class AuthenticationRestEndpoint implements IAuthenticationRestEndpoint {

    @NotNull
    private final AuthenticationService authenticationService;

    @NotNull
    private final JwtTokenUtil jwtTokenUtil;

    @NotNull
    @PostMapping("/login")
    @ApiOperation(
            value = "Sign in process",
            notes = "User sign in process. Authentication request required."
    )
    @Override
    public ResponseEntity<Void> login(
            @ApiParam(
                    name = "authenticationRequest",
                    value = "Authentication user request",
                    required = true
            )
            @RequestBody @Valid final AuthenticationRequestDTO authenticationRequest
    ) throws InvalidParamException {
        @Nullable final String login = authenticationRequest.getLogin();
        @Nullable final String password = authenticationRequest.getPassword();
        @NotNull final SecureUserDTO secureUserDTO = this.authenticationService.signIn(login, password);
        return ResponseEntity.ok()
                .header(
                        HttpHeaders.AUTHORIZATION,
                        this.jwtTokenUtil.generateAccessToken(secureUserDTO)
                )
                .build();
    }

    @NotNull
    @PostMapping("/sign-up")
    @ApiOperation(
            value = "Sign up process",
            notes = "User sign up process. Create user request required."
    )
    @Override
    public ResponseEntity<Void> signUp(
            @ApiParam(
                    name = "createUserRequest",
                    value = "Create user request",
                    required = true
            )
            @RequestBody @Valid @NotNull final CreateUserRequestDTO createUserRequest
    ) throws InvalidParamException {
        @Nullable final String login = createUserRequest.getLogin();
        @Nullable final String password = createUserRequest.getPassword();
        this.authenticationService.signUp(login, password);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .build();
    }

}
