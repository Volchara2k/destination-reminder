package ru.rencredit.volkov.reminder.api.controller.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.volkov.reminder.annotation.Admin;
import ru.rencredit.volkov.reminder.annotation.Auth;

@SuppressWarnings("unused")
public interface IDestinationController {

    @Auth
    @NotNull
    @GetMapping("/")
    ModelAndView subscribersList();

    @Auth
    @NotNull
    @GetMapping("/destination/subscribe")
    ModelAndView subscribe();

    @Auth
    @NotNull
    @PostMapping("/destination/subscribe")
    ModelAndView subscribe(
            @ModelAttribute("email") @NotNull String email,
            @NotNull BindingResult result
    );

    @Auth
    @NotNull
    @GetMapping("/destination/unsubscribe/{email}")
    ModelAndView unsubscribe(
            @PathVariable("email") @NotNull String email
    );

    @Admin
    @NotNull
    @GetMapping("/destinations/remind")
    ModelAndView remind();

}