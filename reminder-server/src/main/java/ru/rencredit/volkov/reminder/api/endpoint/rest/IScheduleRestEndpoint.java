package ru.rencredit.volkov.reminder.api.endpoint.rest;

import io.swagger.annotations.ApiOperation;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.util.Collection;

public interface IScheduleRestEndpoint {

    @NotNull
    @GetMapping("/schedules")
    @ApiOperation(
            value = "Get all schedules",
            notes = "Returns a complete list of schedules details."
    )
    ResponseEntity<Collection<RestSchedulePOJO>> getAllSchedules();

}