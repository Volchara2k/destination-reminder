package ru.rencredit.volkov.reminder.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IScheduleLoaderService;
import ru.rencredit.volkov.reminder.exception.InvalidLoadScheduleException;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.util.ResourceUtil;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import java.io.File;
import java.net.URL;

@Slf4j
@Service
//TODO Прокси не допускает скачивание с ресурса
public class ScheduleLoaderService implements IScheduleLoaderService {

    @NotNull
    private final String spec;

    @NotNull
    private final String cvsPathname;

    public ScheduleLoaderService(
            @Value("${schedule.reminder.spec}") @NotNull final String spec,
            @Value("${schedule.reminder.pathname}") @NotNull final String cvsPathname
    ) {
        this.spec = spec;
        this.cvsPathname = cvsPathname;
    }

    @Override
    public boolean loadToResource(
    ) throws InvalidParamException, InvalidLoadScheduleException {
        return this.loadToResource(this.spec, this.cvsPathname);
    }

    @Override
    public boolean loadToResource(
            @Nullable final String spec, @Nullable final String pathname
    ) throws InvalidParamException, InvalidLoadScheduleException {
        log.info("load to resource {} pending", spec);
        if (ValidRuleUtil.isNullOrEmpty(spec)) {
            log.error("loadToResource error {}", InvalidParamException.class);
            throw new InvalidParamException("spec");
        }
        if (ValidRuleUtil.isNullOrEmpty(pathname)) {
            log.error("loadToResource error {}", InvalidParamException.class);
            throw new InvalidParamException("pathname");
        }

        return this.copyFileFromURL(spec, ResourceUtil.targetFileFor(pathname));
    }

    private boolean copyFileFromURL(
            @NotNull final String spec, @NotNull final File cvsFile
    ) throws InvalidLoadScheduleException {
        try {
            log.info("copyFileFromURL {} process", spec);
            FileUtils.copyURLToFile(new URL(spec), cvsFile);
        } catch (@NotNull final Exception exception) {
            log.error("copyFileFromURL error {}", InvalidLoadScheduleException.class);
            log.info(exception.getLocalizedMessage());
            throw new InvalidLoadScheduleException("Invalid copy file from URL", exception);
        }
        return true;
    }

}