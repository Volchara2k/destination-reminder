package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IScheduleListenerService;
import ru.rencredit.volkov.reminder.api.service.IScheduleParserService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduleListenerService implements IScheduleListenerService {

    @NotNull
    private final IScheduleParserService scheduleParserService;

    @Override
    public boolean todayIsDispatchDay(
    ) throws InvalidParamException, InvalidScheduleYearException, IOException {
        final int localDayOfMonth = LocalDate.now().getDayOfMonth();
        final int localMonthValue = LocalDate.now().getMonthValue();
        final int localYear = LocalDate.now().getYear();
        return this.todayIsDispatchDay(localDayOfMonth, localMonthValue, localYear);
    }

    @Override
    public boolean todayIsDispatchDay(
            final int dayOfMonth, final int monthValue, final int year
    ) throws InvalidParamException, InvalidScheduleYearException, IOException {
        @NotNull final String[] restDays = this.scheduleParserService.parseRestDaysByMonthOfYear(monthValue, year);
        log.info("rest days {}", (Object[]) restDays);
        @NotNull final String localDayOfMonthAsString = String.valueOf(dayOfMonth);
        log.info("today is dispatch day {}", new int[]{dayOfMonth, monthValue, year});
        return !Arrays.asList(restDays).contains(localDayOfMonthAsString);
    }

}