package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.entity.User;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Collection;

public interface IUserService {

    @NotNull
    @SuppressWarnings("unused")
    User createUser(
            @Nullable String login,
            @Nullable String password
    ) throws InvalidParamException;

    @Nullable
    User getUserByLogin(
            @Nullable String login
    ) throws InvalidParamException;

    @NotNull
    @PostConstruct
    Collection<User> initialDemoData(
    ) throws InvalidParamException, IOException;

}