package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.exception.InvalidSendEmailException;

import java.util.Collection;

public interface INotificatorService<T> {

    boolean notifySubscribers(
    ) throws InvalidAddresseeException, InvalidSendEmailException;

    boolean notifySubscribers(
            @Nullable Collection<T> addressees
    ) throws InvalidAddresseeException, InvalidSendEmailException;

}