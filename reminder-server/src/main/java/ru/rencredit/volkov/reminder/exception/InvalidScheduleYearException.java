package ru.rencredit.volkov.reminder.exception;

public final class InvalidScheduleYearException extends AbstractException {

    public InvalidScheduleYearException() {
        super("Год некорректен или не обнаружен!");
    }

}