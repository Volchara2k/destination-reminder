package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.exception.InvalidSendEmailException;

public interface ISenderService<T> {

    boolean sendTo(
            @Nullable T[] to
    ) throws InvalidAddresseeException, InvalidSendEmailException;

}