package ru.rencredit.volkov.reminder.domain.pojo;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class RestSchedulePOJO {

    @CsvBindByName(column = "Год/Месяц")
    private int year;

    @NotNull
    @CsvBindByName(column = "Январь")
    private String[] januaryDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Февраль")
    private String[] februaryDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Март")
    private String[] marchDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Апрель")
    private String[] aprilDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Май")
    private String[] mayDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Июнь")
    private String[] juneDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Июль")
    private String[] juleDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Август")
    private String[] augustDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Сентябрь")
    private String[] septemberDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Октябрь")
    private String[] octoberDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Ноябрь")
    private String[] novemberDates = new String[]{};

    @NotNull
    @CsvBindByName(column = "Декабрь")
    private String[] decemberDates = new String[]{};

}