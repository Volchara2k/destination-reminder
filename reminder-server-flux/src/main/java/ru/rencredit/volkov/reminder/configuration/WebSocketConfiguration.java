package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.reactive.HandlerMapping;
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.server.WebSocketService;
import org.springframework.web.reactive.socket.server.support.HandshakeWebSocketService;
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter;
import org.springframework.web.reactive.socket.server.upgrade.ReactorNettyRequestUpgradeStrategy;
import ru.rencredit.volkov.reminder.handler.DestinationReactiveCreateHandler;
import ru.rencredit.volkov.reminder.handler.DestinationReactiveDeleteHandler;
import ru.rencredit.volkov.reminder.handler.DestinationReactiveListHandler;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
public class WebSocketConfiguration {

    @NotNull
    private final DestinationReactiveListHandler destinationReactiveListHandler;

    @NotNull
    private final DestinationReactiveCreateHandler destinationReactiveCreateHandler;

    @NotNull
    private final DestinationReactiveDeleteHandler destinationReactiveDeleteHandler;

    public WebSocketConfiguration(
            @NotNull final DestinationReactiveListHandler destinationReactiveListHandler,
            @NotNull final DestinationReactiveCreateHandler destinationReactiveCreateHandler,
            @NotNull final DestinationReactiveDeleteHandler destinationReactiveDeleteHandler
    ) {
        this.destinationReactiveListHandler = destinationReactiveListHandler;
        this.destinationReactiveCreateHandler = destinationReactiveCreateHandler;
        this.destinationReactiveDeleteHandler = destinationReactiveDeleteHandler;
    }

    @Bean
    @NotNull
    public HandlerMapping handlerMapping() {
        @NotNull final SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping(
                this.webSocketMap(), Ordered.HIGHEST_PRECEDENCE
        );
        return mapping;
    }

    @Bean
    @NotNull
    public WebSocketHandlerAdapter handlerAdapter(
            @NotNull final WebSocketService webSocketService
    ) {
        return new WebSocketHandlerAdapter(webSocketService);
    }

    @Bean
    @NotNull
    public WebSocketService webSocketService() {
        return new HandshakeWebSocketService(
                new ReactorNettyRequestUpgradeStrategy()
        );
    }

    @NotNull
    private Map<String, WebSocketHandler> webSocketMap() {
        return Stream.of(
                new Object[][]{
                        {"/destinations", this.destinationReactiveListHandler},
                        {"/create", this.destinationReactiveCreateHandler},
                        {"/delete", this.destinationReactiveDeleteHandler},
                }
        )
                .collect(Collectors.toMap(data -> (String) data[0], data -> (WebSocketHandler) data[1]));
    }

}