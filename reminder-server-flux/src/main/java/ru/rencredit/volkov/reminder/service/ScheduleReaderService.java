package ru.rencredit.volkov.reminder.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.ISchedulerReaderService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.io.IOException;
import java.util.Collection;

@Slf4j
@Service
@SuppressWarnings("unused")
public class ScheduleReaderService extends AbstractCvsReaderService<RestSchedulePOJO> implements ISchedulerReaderService {

    @NotNull
    private final String cvsPathname;

    public ScheduleReaderService(
            @Value("${schedule.reminder.pathname}") @NotNull final String cvsPathname
    ) {
        super(RestSchedulePOJO.class);
        this.cvsPathname = cvsPathname;
    }

    @NotNull
    @Override
    public Collection<RestSchedulePOJO> readFromResources(
    ) throws InvalidParamException, IOException {
        return super.readFromResources(this.cvsPathname);
    }

}