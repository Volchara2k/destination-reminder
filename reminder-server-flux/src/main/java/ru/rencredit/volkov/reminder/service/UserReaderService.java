package ru.rencredit.volkov.reminder.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IUserReaderService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.pojo.UserPOJO;

import java.io.IOException;
import java.util.Collection;

@Slf4j
@Service
@SuppressWarnings("unused")
public class UserReaderService extends AbstractCvsReaderService<UserPOJO> implements IUserReaderService {

    @NotNull
    private final String cvsPathname;

    public UserReaderService(
            @Value("${user.pathname}") @NotNull final String cvsPathname
    ) {
        super(UserPOJO.class);
        this.cvsPathname = cvsPathname;
    }

    @NotNull
    @Override
    public Collection<UserPOJO> readFromResources(
    ) throws InvalidParamException, IOException {
        return super.readFromResources(this.cvsPathname);
    }

}