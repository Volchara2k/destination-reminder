package ru.rencredit.volkov.reminder.exception;

import org.jetbrains.annotations.Nullable;

public final class InvalidScheduleYearException extends AbstractException {

    public InvalidScheduleYearException(@Nullable final String message) {
        super(message);
    }

}