package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rencredit.volkov.reminder.api.service.IDestinationReaderService;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.entity.Destination;
import ru.rencredit.volkov.reminder.domain.pojo.DestinationPOJO;
import ru.rencredit.volkov.reminder.repository.IDestinationRepository;
import ru.rencredit.volkov.reminder.util.AdapterDestinationUtil;
import ru.rencredit.volkov.reminder.util.ValidRuleUtil;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(
        rollbackFor = Exception.class
)
public class DestinationService implements IDestinationService {

    @NotNull
    private final IDestinationRepository destinationRepository;

    @NotNull
    private final IDestinationReaderService destinationReaderService;

    @NotNull
    @Override
    public Mono<Destination> subscribe(@Nullable final String email) throws InvalidParamException {
        log.info("subscribe {} pending", email);
        if (ValidRuleUtil.isNullOrEmpty(email)) {
            log.error("subscribe error {}", InvalidParamException.class);
            throw new InvalidParamException("email");
        }

        log.info("subscribe {} process", email);
        @NotNull final Destination destination = new Destination(email);
        return Mono.just(destination)
                .flatMap(this.destinationRepository::insert);
    }

    @NotNull
    @Override
    public Mono<String> unsubscribeForResult(@Nullable final String email) throws InvalidParamException {
        log.info("unsubscribe {} pending", email);
        if (ValidRuleUtil.isNullOrEmpty(email)) {
            log.error("unsubscribe error {}", InvalidParamException.class);
            throw new InvalidParamException("email");
        }

        this.unsubscribe(email)
                .toFuture()
                .join();
        return Mono.just(email);
    }

    @NotNull
    @Override
    public Mono<Integer> unsubscribe(@Nullable final String email) throws InvalidParamException {
        log.info("unsubscribe {} pending", email);
        if (ValidRuleUtil.isNullOrEmpty(email)) {
            log.error("unsubscribe error {}", InvalidParamException.class);
            throw new InvalidParamException("email");
        }

        log.info("unsubscribe {} process", email);
        return Mono.just(email)
                .flatMap(this.destinationRepository::deleteByEmail);
    }

    @NotNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public Flux<String> getAllDestinationsEmail() {
        log.info("get all destination emails");
        return this.getAllDestinations()
                .map(Destination::getEmail);
    }

    @Nullable
    @Override
    public Mono<Destination> getDestinationById(
            @Nullable final String id
    ) throws InvalidParamException {
        if (ValidRuleUtil.isNullOrEmpty(id)) throw new InvalidParamException("id");
        return this.destinationRepository.findById(id);
    }

    @NotNull
    @Transactional(
            rollbackFor = Exception.class,
            readOnly = true
    )
    @Override
    public Flux<Destination> getAllDestinations() {
        log.info("get all destinations");
        return this.destinationRepository.findAll();
    }

    @SneakyThrows
    @PostConstruct
    @Override
    public void initialDemoData() {
        log.info("initial demo data pending");
        final long existDataCount = this.destinationRepository.count()
                .toFuture()
                .join();
        final boolean isNotExistData = existDataCount == 0;

        if (isNotExistData) {
            @NotNull final Collection<DestinationPOJO> destinationsPOJO = this.destinationReaderService.readFromResources();
            log.info("initial demo data process {}", destinationsPOJO);
            @NotNull final Collection<Destination> destinations = AdapterDestinationUtil.forDestinations.apply(destinationsPOJO);
            this.destinationRepository.insert(destinations).subscribe();
            log.info("initial demo data fine {}", destinationsPOJO);
        }
    }

}