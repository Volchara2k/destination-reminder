package ru.rencredit.volkov.reminder.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.api.service.IEmailNotificatorService;
import ru.rencredit.volkov.reminder.api.service.IEmailSenderService;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.exception.InvalidSendEmailException;

import java.util.Collection;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmailNotificatorService extends AbstractEmailNotificatorService implements IEmailNotificatorService {

    @NotNull
    private final IDestinationService destinationService;

    public EmailNotificatorService(
            @NotNull final IEmailSenderService emailSenderService,
            @NotNull final IDestinationService destinationService
    ) {
        super(emailSenderService);
        this.destinationService = destinationService;
    }

    @Override
    public boolean notifySubscribers(
    ) throws InvalidAddresseeException, InvalidSendEmailException {
        @NotNull final Collection<String> destinationsEmail = this.destinationService.getAllDestinationsEmail()
                .toStream()
                .collect(Collectors.toList());
        return super.notifySubscribers(destinationsEmail);
    }

}