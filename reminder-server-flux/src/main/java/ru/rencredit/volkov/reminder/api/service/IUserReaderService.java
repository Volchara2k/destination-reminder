package ru.rencredit.volkov.reminder.api.service;

import ru.rencredit.volkov.reminder.domain.pojo.UserPOJO;

public interface IUserReaderService extends ICsvReaderService<UserPOJO> {
}