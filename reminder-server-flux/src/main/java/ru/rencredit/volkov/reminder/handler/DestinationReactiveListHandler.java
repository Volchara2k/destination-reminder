package ru.rencredit.volkov.reminder.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

@Slf4j
@Component
@RequiredArgsConstructor
public class DestinationReactiveListHandler implements WebSocketHandler {

    @NotNull
    private final IDestinationService destinationService;

    @NotNull
    @Override
    public Mono<Void> handle(@NotNull final WebSocketSession webSocketSession) {
        return webSocketSession.send(
                this.destinationService.getAllDestinations()
                        .map(Destination::getEmail)
                        .map(webSocketSession::textMessage));
    }

}
