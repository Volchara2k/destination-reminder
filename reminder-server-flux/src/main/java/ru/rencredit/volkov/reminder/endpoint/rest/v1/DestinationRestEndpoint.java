package ru.rencredit.volkov.reminder.endpoint.rest.v1;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rencredit.volkov.reminder.api.endpoint.rest.IDestinationRestEndpoint;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.NotFoundDestinationException;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

import java.util.Objects;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(
        value = "/api/v1",
        produces = MediaType.APPLICATION_JSON_VALUE
)
public final class DestinationRestEndpoint implements IDestinationRestEndpoint {

    @NotNull
    private final IDestinationService destinationService;

    @NotNull
    @GetMapping("/destinations")
    @ApiOperation(
            value = "Get all destinations",
            notes = "Returns a complete list of destination details."
    )
    @Override
    public ResponseEntity<Flux<Destination>> getAllDestination() {
        log.info("get all destinations");
        return ResponseEntity.ok(
                this.destinationService.getAllDestinations()
        );
    }

    @NotNull
    @GetMapping("/destinations/emails")
    @ApiOperation(
            value = "Get all destination emails",
            notes = "Returns a complete list of destination emails details."
    )
    @Override
    public ResponseEntity<Flux<String>> getAllDestinationsEmail() {
        log.info("get all destinations email");
        return ResponseEntity.ok(
                this.destinationService.getAllDestinationsEmail()
        );
    }

    @NotNull
    @GetMapping(
            value = "/destination/{id}"
    )
    @ApiOperation(
            value = "Get destination by unique ID",
            notes = "Return destination by unique UD. Unique ID required."
    )
    @Override
    public ResponseEntity<Mono<Destination>> getDestinationById(
            @ApiParam(
                    name = "id",
                    value = "Unique ID of destination",
                    example = "432roijr32iojr32rio3j",
                    readOnly = true
            )
            @PathVariable("id") @NotNull final String id
    ) throws InvalidParamException, NotFoundDestinationException {
        @Nullable final Mono<Destination> destination = this.destinationService.getDestinationById(id);
        if (Objects.isNull(destination)) throw new NotFoundDestinationException();
        return ResponseEntity.ok(
                destination
        );
    }

    @NotNull
    @PostMapping(
            value = "/destination/subscribe"
    )
    @ApiOperation(
            value = "Subscribe destination",
            notes = "Returns subscribed destination. Created destination required."
    )
    @Override
    public ResponseEntity<Mono<Destination>> subscribeDestination(
            @ApiParam(
                    name = "email",
                    value = "Unique email for destination",
                    example = "example@rencredit.ru",
                    required = true
            )
            @RequestBody @NotNull final String email
    ) throws InvalidParamException {
        log.info("subscribe destination {}", email);
        return ResponseEntity.ok(
                this.destinationService.subscribe(email)
        );
    }

    @NotNull
    @DeleteMapping("/destination/unsubscribe/{email}")
    @ApiOperation(
            value = "Unsubscribe destination by email",
            notes = "Returns integer unsubscribed flag: 1 - true, 0 - false. Unique email required."
    )
    @Override
    public ResponseEntity<Mono<Integer>> unsubscribeDestination(
            @ApiParam(
                    name = "email",
                    value = "Unique email for destination",
                    example = "example@rencredit.ru",
                    required = true
            )
            @PathVariable("email") @NotNull final String email
    ) throws InvalidParamException {
        log.info("unsubscribeDestination destination {}", email);
        return ResponseEntity.ok(
                this.destinationService.unsubscribe(email)
        );
    }

}