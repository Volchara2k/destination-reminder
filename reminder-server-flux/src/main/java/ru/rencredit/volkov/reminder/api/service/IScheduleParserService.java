package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;

import java.io.IOException;

public interface IScheduleParserService {

    @NotNull
    String[] parseRestDaysByMonthOfYear(
            int monthValue, int year
    ) throws InvalidParamException, InvalidScheduleYearException, IOException;

}