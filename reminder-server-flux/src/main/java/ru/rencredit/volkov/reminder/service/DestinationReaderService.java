package ru.rencredit.volkov.reminder.service;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IDestinationReaderService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.pojo.DestinationPOJO;

import java.io.IOException;
import java.util.Collection;

@Slf4j
@Service
@SuppressWarnings("unused")
public class DestinationReaderService extends AbstractCvsReaderService<DestinationPOJO> implements IDestinationReaderService {

    @NotNull
    private final String cvsPathname;

    public DestinationReaderService(
            @Value("${destination.pathname}") @NotNull final String cvsPathname
    ) {
        super(DestinationPOJO.class);
        this.cvsPathname = cvsPathname;
    }

    @NotNull
    @Override
    public Collection<DestinationPOJO> readFromResources(
    ) throws InvalidParamException, IOException {
        return super.readFromResources(this.cvsPathname);
    }

}