package ru.rencredit.volkov.reminder.enumeration;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public enum UserRoleType {

    ADMIN("Администратор"),

    USER("Пользователь");

    @NotNull
    private final String title;

    UserRoleType(@NotNull final String title) {
        this.title = title;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

}