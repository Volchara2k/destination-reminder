package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.api.service.IEmailNotificatorService;
import ru.rencredit.volkov.reminder.api.service.IEmailSenderService;
import ru.rencredit.volkov.reminder.exception.InvalidAddresseeException;
import ru.rencredit.volkov.reminder.exception.InvalidSendEmailException;

import java.util.Collection;
import java.util.Objects;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractEmailNotificatorService implements IEmailNotificatorService {

    @NotNull
    private final IEmailSenderService emailSenderService;

    @Override
    public boolean notifySubscribers(
            @Nullable final Collection<String> addressees
    ) throws InvalidAddresseeException, InvalidSendEmailException {
        log.info("notify subscribers pending {}", addressees);
        if (Objects.isNull(addressees)) {
            log.error("notify subscribers error {}", InvalidAddresseeException.class);
            throw new InvalidAddresseeException();
        }

        @NotNull final String[] destinationsEmailAsArray = addressees.toArray(new String[0]);
        log.info("notify subscribers process {} elements", destinationsEmailAsArray.length);
        return this.notifyProcessFor(destinationsEmailAsArray);
    }

    private boolean notifyProcessFor(
            @NotNull final String[] emails
    ) throws InvalidAddresseeException, InvalidSendEmailException {
        log.info("notify process for {} elements", emails.length);
        final boolean isFaultSending = !this.emailSenderService.sendTo(emails);
        if (isFaultSending) return false;
        log.info("notify process for {} elements fine", emails.length);
        return true;
    }

}