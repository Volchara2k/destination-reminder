package ru.rencredit.volkov.reminder.exception;

import org.jetbrains.annotations.NotNull;

public final class InvalidParamException extends AbstractException {

    public InvalidParamException(@NotNull final String message) {
        super(
                String.format(
                        "Обнаружен недопустимый параметр: %s. Проверьте корректность вводимых данных!",
                        message
                )
        );
    }

}