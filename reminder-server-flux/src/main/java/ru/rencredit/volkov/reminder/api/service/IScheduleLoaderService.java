package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.volkov.reminder.exception.InvalidLoadScheduleException;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;

public interface IScheduleLoaderService {

    @SuppressWarnings("unused")
    boolean loadToResource(
    ) throws InvalidParamException, InvalidLoadScheduleException;

    boolean loadToResource(
            @Nullable String spec, @Nullable String pathname
    ) throws InvalidParamException, InvalidLoadScheduleException;

}