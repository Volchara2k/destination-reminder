package ru.rencredit.volkov.reminder.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import ru.rencredit.volkov.reminder.enumeration.UserRoleType;
import ru.rencredit.volkov.reminder.domain.entity.User;
import ru.rencredit.volkov.reminder.domain.pojo.UserPOJO;

import java.util.Collection;
import java.util.LinkedList;
import java.util.function.Function;

@Slf4j
@UtilityClass
public class AdapterUserUtil {

    @NotNull
    public Function<Collection<UserPOJO>, Collection<User>> forUsers = usersPOJO -> {
        @NotNull final Collection<User> destinations = new LinkedList<>();
        log.info("conversion process {} pending", usersPOJO);
        usersPOJO.forEach(userPOJO -> destinations.add(
                User.builder()
                        .id(userPOJO.getId())
                        .login(userPOJO.getLogin())
                        .passwordHash(userPOJO.getPasswordHash())
                        .lockdown(userPOJO.getLockdown())
                        .userRoleType(
                                UserRoleType.valueOf(
                                        userPOJO.getUserRoleType()
                                )
                        )
                        .build()
        ));
        log.info("conversion process {} fine", destinations);
        return destinations;
    };

}