package ru.rencredit.volkov.reminder.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@UtilityClass
public class ScheduleDateUtil {

    @NotNull
    public String[] parseRestDaysByMonthValue(@NotNull final RestSchedulePOJO schedule, final int monthValue) {
        log.info("parse rest days {} by month value {}", schedule, monthValue);
        return Stream.of(
                new Object[][]{
                        {1, schedule.getJanuaryDates()},
                        {2, schedule.getFebruaryDates()},
                        {3, schedule.getMarchDates()},
                        {4, schedule.getAprilDates()},
                        {5, schedule.getMayDates()},
                        {6, schedule.getJuneDates()},
                        {7, schedule.getJuleDates()},
                        {8, schedule.getAugustDates()},
                        {9, schedule.getSeptemberDates()},
                        {10, schedule.getOctoberDates()},
                        {11, schedule.getNovemberDates()},
                        {12, schedule.getDecemberDates()},
                }
        )
                .collect(Collectors.toMap(data -> (Integer) data[0], data -> (String[]) data[1]))
                .get(monthValue);
    }

}