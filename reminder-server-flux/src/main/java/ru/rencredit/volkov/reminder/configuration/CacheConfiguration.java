package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
@EnableCaching
public class CacheConfiguration {

    @NotNull
    private final String schedulesValues;

    @NotNull
    private final String scheduleValues;

    public CacheConfiguration(
            @Value("${cacheable.values.schedules}") @NotNull final String schedulesValues,
            @Value("${cacheable.values.schedule}") @NotNull final String scheduleValues
    ) {
        this.schedulesValues = schedulesValues;
        this.scheduleValues = scheduleValues;
    }

    @Bean
    @NotNull
    public CacheManager cacheManager() {
        @NotNull final SimpleCacheManager cacheManager = new SimpleCacheManager();
        cacheManager.setCaches(
                Arrays.asList(
                        new ConcurrentMapCache(this.schedulesValues),
                        new ConcurrentMapCache(this.scheduleValues)
                )
        );
        return cacheManager;
    }

}