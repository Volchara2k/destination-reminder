package ru.rencredit.volkov.reminder.domain.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;
import ru.rencredit.volkov.reminder.enumeration.UserRoleType;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "users")
@ApiModel(description = "All details about the user")
public final class User {

    @NotNull
    @Field
    @MongoId
    @Builder.Default
    @ApiModelProperty(
            value = "Unique ID of user",
            example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
            name = "id",
            required = true
    )
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Field
    @Builder.Default
    @Indexed(unique = true)
    @ApiModelProperty(
            value = "Unique login of user",
            example = "volkov",
            name = "login",
            required = true
    )
    private String login = "";

    @NotNull
    @Field
    @Builder.Default
    @ApiModelProperty(
            value = "Password hash of user",
            example = "7ca5a0e6%5f22%4955%b134%42f826a2d8dc",
            name = "passwordHash",
            required = true
    )
    private String passwordHash = "";

    @NotNull
    @Field
    @Builder.Default
    @ApiModelProperty(
            value = "User lockdown",
            example = "false",
            name = "lockdown",
            required = true
    )
    private Boolean lockdown = false;

    @NotNull
    @Field
    @Builder.Default
    @ApiModelProperty(
            value = "User role",
            example = "USER",
            name = "userRoleType",
            required = true
    )
    private UserRoleType userRoleType = UserRoleType.USER;

    public User(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this();
        setLogin(login);
        setPasswordHash(passwordHash);
    }

}