package ru.rencredit.volkov.reminder.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@Slf4j
@UtilityClass
public class ResourceUtil {

    @NotNull
    //TODO REPLACE
    private static final String MODULE_RESOURCE_PATH =
            StringUtils.join(
                    File.separator,
                    "reminder-server-flux",
                    File.separator,
                    "src",
                    File.separator,
                    "main",
                    File.separator,
                    "resources",
                    File.separator
            );

    @NotNull
    public File targetFileFor(@NotNull final String pathname) {
        log.info("resource file for {} pending", pathname);
        @NotNull final String rootPath = targetPathFor(pathname);
        log.info("resource file for {} process", rootPath);
        return new File(rootPath);
    }

    @NotNull
    public String targetPathFor(@NotNull final String pathname) {
        log.info("resource path for {} pending", pathname);
        @NotNull final String rootPath = rootPath();
        log.info("resource path for {} process", rootPath);
        return StringUtils.join(
                rootPath,
                MODULE_RESOURCE_PATH,
                pathname
        );
    }

    @NotNull
    public String rootPath() {
        @NotNull final String rootPath = System.getProperty("user.dir");
        log.info("root path {}", rootPath);
        return rootPath;
    }

    @NotNull
    public File sourceFileFrom(@NotNull final String pathname) throws FileNotFoundException {
        log.info("resource file from {} pending", pathname);
        @NotNull final String formatPathName = classpathFrom(pathname);
        log.info("resource file from {} process", formatPathName);
        try {
            return ResourceUtils.getFile(formatPathName);
        } catch (@NotNull final FileNotFoundException exception) {
            log.error("resource file from error {}", FileNotFoundException.class);
            log.info(exception.getLocalizedMessage());
            throw new FileNotFoundException("Invalid pathname");
        }
    }

    @NotNull
    public String classpathFrom(@NotNull final String pathname) {
        return StringUtils.join(ResourceLoader.CLASSPATH_URL_PREFIX, pathname);
    }

}