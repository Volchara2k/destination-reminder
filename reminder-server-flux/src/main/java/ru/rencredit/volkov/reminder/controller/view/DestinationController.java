package ru.rencredit.volkov.reminder.controller.view;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring5.context.webflux.ReactiveDataDriverContextVariable;
import reactor.core.publisher.Flux;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

@Controller
@RequiredArgsConstructor
public final class DestinationController {

    @NotNull
    private final IDestinationService destinationService;

    @NotNull
    @GetMapping("/")
    public String destinations(@NotNull final Model model) {
        @NotNull final Flux<Destination> destinations = this.destinationService.getAllDestinations();
        model.addAttribute("destinations", new ReactiveDataDriverContextVariable(destinations));
        return "index";
    }

}