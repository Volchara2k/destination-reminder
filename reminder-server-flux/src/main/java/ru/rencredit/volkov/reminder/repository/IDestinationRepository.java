package ru.rencredit.volkov.reminder.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

public interface IDestinationRepository extends ReactiveMongoRepository<Destination, String> {

    @NotNull
    Mono<Integer> deleteByEmail(@NotNull String email);

}