package ru.rencredit.volkov.reminder.exception;

public final class NotFoundDestinationException extends AbstractException {

    public NotFoundDestinationException() {
        super("Пользователь не найден!");
    }

}