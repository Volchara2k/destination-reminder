package ru.rencredit.volkov.reminder.domain.pojo;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public final class UserPOJO {

    @NotNull
    @CsvBindByName(column = "Идентификатор")
    private String id = "";

    @NotNull
    @CsvBindByName(column = "Логин")
    private String login = "";

    @NotNull
    @CsvBindByName(column = "Хеш-пароля")
    private String passwordHash = "";

    @NotNull
    @CsvBindByName(column = "Заблокирован")
    private Boolean lockdown = false;

    @NotNull
    @CsvBindByName(column = "Права")
    private String userRoleType = "";

}