package ru.rencredit.volkov.reminder.exception;

import org.jetbrains.annotations.Nullable;

@SuppressWarnings("unused")
public abstract class AbstractException extends Exception {

    AbstractException(@Nullable final String message) {
        super(message);
    }

    AbstractException(@Nullable final String message, @Nullable final Throwable cause) {
        super(message, cause);
    }

    AbstractException(@Nullable final Throwable cause) {
        super(cause);
    }

}