package ru.rencredit.volkov.reminder.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.domain.entity.Destination;

import javax.annotation.PostConstruct;

public interface IDestinationService {

    @NotNull
    Mono<Destination> subscribe(@Nullable String email) throws InvalidParamException;

    @NotNull
    Mono<String> unsubscribeForResult(@Nullable String email) throws InvalidParamException;

    @NotNull
    Mono<Integer> unsubscribe(@Nullable String email) throws InvalidParamException;

    @NotNull
    Flux<String> getAllDestinationsEmail();

    @Nullable
    Mono<Destination> getDestinationById(String id) throws InvalidParamException;

    @NotNull
    Flux<Destination> getAllDestinations();

    @SuppressWarnings("unused")
    @PostConstruct
    void initialDemoData();

}