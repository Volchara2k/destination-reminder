package ru.rencredit.volkov.reminder.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
public final class ApiExceptionResponseDTO {

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    private LocalDateTime timestamp;

    @NotNull
    private HttpStatus status;

    @NotNull
    private String errorMessage;

    public ApiExceptionResponseDTO(
            @NotNull final HttpStatus status,
            @NotNull final Exception throwable
    ) {
        this.timestamp = LocalDateTime.now();
        this.status = status;
        this.errorMessage = throwable.getMessage();
    }

}