package ru.rencredit.volkov.reminder.domain.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

import javax.validation.constraints.Email;
import java.util.UUID;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="destinations")
@ApiModel(description = "All details about the destination")
public final class Destination {

    @NotNull
    @Field
    @MongoId
    @Builder.Default
    @ApiModelProperty(
            value = "Unique ID of destination",
            example = "7ca5a0e6-5f22-4955-b134-42f826a2d8dc",
            name = "id",
            required = true
    )
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Field
    @Email(
            regexp = "([a-zA-Z0-9]+)([.{1}])?([a-zA-Z0-9]+)@rencredit([.])ru",
            message = "Недопустимый формат электронной почты! Проверьте корректность вводимых данных: @rencredit.ru"
    )
    @Builder.Default
    @Indexed(unique = true)
    @ApiModelProperty(
            value = "Email of the destination",
            name = "email",
            example = "vvolkov@rencredit.ru",
            required = true
    )
    private String email = "vvolkov@rencredit.ru";

    public Destination(@NotNull final String email) {
        setId(UUID.randomUUID().toString());
        setEmail(email);
    }

}