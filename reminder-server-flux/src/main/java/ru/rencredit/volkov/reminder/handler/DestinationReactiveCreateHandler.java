package ru.rencredit.volkov.reminder.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.socket.WebSocketHandler;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.WebSocketSession;
import reactor.core.publisher.Mono;
import ru.rencredit.volkov.reminder.api.service.IDestinationService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;

@Slf4j
@Component
@RequiredArgsConstructor
public class DestinationReactiveCreateHandler implements WebSocketHandler {

    @NotNull
    private final IDestinationService destinationService;

    @NotNull
    @Override
    public Mono<Void> handle(@NotNull final WebSocketSession webSocketSession) {
        return webSocketSession.send(webSocketSession.receive()
                .map(WebSocketMessage::getPayloadAsText)
                .flatMap(email -> {
                            try {
                                log.info("handle {}", email);
                                return this.destinationService.subscribe(email);
                            } catch (@NotNull final InvalidParamException exception) {
                                log.error("handle error {}", InvalidParamException.class);
                                log.info(exception.getLocalizedMessage());
                                return Mono.empty();
                            }
                        }
                )
                .map(destination -> webSocketSession.textMessage(destination.getEmail()))
        );
    }

}
