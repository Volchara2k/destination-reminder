package ru.rencredit.volkov.reminder.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.rencredit.volkov.reminder.api.service.IScheduleParserService;
import ru.rencredit.volkov.reminder.api.service.IScheduleService;
import ru.rencredit.volkov.reminder.exception.InvalidParamException;
import ru.rencredit.volkov.reminder.exception.InvalidScheduleYearException;
import ru.rencredit.volkov.reminder.domain.pojo.RestSchedulePOJO;
import ru.rencredit.volkov.reminder.util.ScheduleDateUtil;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class ScheduleParserService implements IScheduleParserService {

    @NotNull
    private final IScheduleService scheduleService;

    @NotNull
    @Override
    public String[] parseRestDaysByMonthOfYear(
            final int monthValue, final int year
    ) throws InvalidParamException, InvalidScheduleYearException, IOException {
        @NotNull final RestSchedulePOJO schedule = this.scheduleService.getScheduleByYear(year);
        log.info("parse schedule {} by month {} of year {}", schedule, monthValue, year);
        return ScheduleDateUtil.parseRestDaysByMonthValue(schedule, monthValue);
    }

}