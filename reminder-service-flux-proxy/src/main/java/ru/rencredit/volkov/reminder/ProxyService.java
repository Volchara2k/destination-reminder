package ru.rencredit.volkov.reminder;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class ProxyService {

    public static void main(@Nullable final String[] args) {
        SpringApplication.run(ProxyService.class, args);
    }

}