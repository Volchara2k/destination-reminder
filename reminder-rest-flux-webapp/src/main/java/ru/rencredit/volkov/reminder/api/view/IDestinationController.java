package ru.rencredit.volkov.reminder.api.view;

import org.jetbrains.annotations.NotNull;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@SuppressWarnings("unused")
public interface IDestinationController {

    @NotNull
    @GetMapping("/")
    ModelAndView subscribers();

    @NotNull
    @GetMapping("/destination/subscribe")
    ModelAndView subscribe();

    @NotNull
    @PostMapping("/destination/subscribe")
    ModelAndView subscribe(
            @ModelAttribute("email") @NotNull String email,
            @NotNull BindingResult result
    );

    @NotNull
    @GetMapping("/destination/unsubscribe/{email}")
    ModelAndView unsubscribe(
            @PathVariable("email") @NotNull String email
    );

}