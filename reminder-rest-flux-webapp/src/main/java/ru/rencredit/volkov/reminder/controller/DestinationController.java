package ru.rencredit.volkov.reminder.controller;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.volkov.reminder.api.DefaultApi;
import ru.rencredit.volkov.reminder.api.view.IDestinationController;
import ru.rencredit.volkov.reminder.domain.Destination;

import java.util.Collection;

@Controller
@RequiredArgsConstructor
public final class DestinationController implements IDestinationController {

    @NotNull
    private final DefaultApi defaultApi;

    @NotNull
    @GetMapping("/")
    @Override
    public ModelAndView subscribers() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("index");
        final Collection<Destination> destinations = this.defaultApi.getAllDestination();
        modelAndView.addObject("destinations", destinations);
        return modelAndView;
    }

    @NotNull
    @GetMapping("/destination/subscribe")
    @Override
    public ModelAndView subscribe() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("destination-insert");
        modelAndView.addObject("email", "");
        return modelAndView;
    }

    @NotNull
    @PostMapping("/destination/subscribe")
    @Override
    public ModelAndView subscribe(
            @ModelAttribute("email") @NotNull final String email,
            @NotNull final BindingResult result
    ) {
        this.defaultApi.subscribeDestination(email);
        return new ModelAndView("redirect:/");
    }

    @NotNull
    @GetMapping("/destination/unsubscribe/{email}")
    @Override
    public ModelAndView unsubscribe(
            @PathVariable("email") @NotNull final String email
    ) {
        this.defaultApi.unsubscribeDestination(email);
        return new ModelAndView("redirect:/");
    }

}