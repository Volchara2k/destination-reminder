
# Destination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique ID of destination | 
**email** | **String** | Email of the destination | 



