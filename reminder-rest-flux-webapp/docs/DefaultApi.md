# DefaultApi

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllDestination**](DefaultApi.md#getAllDestination) | **GET** /api/v1/destinations | Get all destinations
[**getAllDestinationsEmail**](DefaultApi.md#getAllDestinationsEmail) | **GET** /api/v1/destinations/emails | Get all destination emails
[**getDestinationById**](DefaultApi.md#getDestinationById) | **GET** /api/v1/destination/{id} | Get destination by unique ID
[**login**](DefaultApi.md#login) | **POST** /api/v1/login | Sign in process
[**signUp**](DefaultApi.md#signUp) | **POST** /api/v1/sign-up | Sign up process
[**subscribeDestination**](DefaultApi.md#subscribeDestination) | **POST** /api/v1/destination/subscribe | Subscribe destination
[**unsubscribeDestination**](DefaultApi.md#unsubscribeDestination) | **DELETE** /api/v1/destination/unsubscribe/{email} | Unsubscribe destination by email


<a name="getAllDestination"></a>
# **getAllDestination**
> List&lt;Destination&gt; getAllDestination()

Get all destinations

Returns a complete list of destination details.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<Destination> result = apiInstance.getAllDestination();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAllDestination");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List&lt;Destination&gt;**](Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAllDestinationsEmail"></a>
# **getAllDestinationsEmail**
> List&lt;String&gt; getAllDestinationsEmail()

Get all destination emails

Returns a complete list of destination emails details.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    List<String> result = apiInstance.getAllDestinationsEmail();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAllDestinationsEmail");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**List&lt;String&gt;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDestinationById"></a>
# **getDestinationById**
> Destination getDestinationById(id)

Get destination by unique ID

Return destination by unique UD. Unique ID required.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "432roijr32iojr32rio3j"; // String | Unique ID of destination
try {
    Destination result = apiInstance.getDestinationById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getDestinationById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Unique ID of destination |

### Return type

[**Destination**](Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="login"></a>
# **login**
> login(authenticationRequest)

Sign in process

User sign in process. Authentication request required.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
AuthenticationRequestDTO authenticationRequest = new AuthenticationRequestDTO(); // AuthenticationRequestDTO | Authentication user request
try {
    apiInstance.login(authenticationRequest);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#login");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authenticationRequest** | [**AuthenticationRequestDTO**](AuthenticationRequestDTO.md)| Authentication user request |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="signUp"></a>
# **signUp**
> signUp(createUserRequest)

Sign up process

User sign up process. Create user request required.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
CreateUserRequestDTO createUserRequest = new CreateUserRequestDTO(); // CreateUserRequestDTO | Create user request
try {
    apiInstance.signUp(createUserRequest);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#signUp");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createUserRequest** | [**CreateUserRequestDTO**](CreateUserRequestDTO.md)| Create user request |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="subscribeDestination"></a>
# **subscribeDestination**
> Destination subscribeDestination(email)

Subscribe destination

Returns subscribed destination. Created destination required.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String email = "email_example"; // String | Unique email for destination
try {
    Destination result = apiInstance.subscribeDestination(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#subscribeDestination");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| Unique email for destination |

### Return type

[**Destination**](Destination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="unsubscribeDestination"></a>
# **unsubscribeDestination**
> Integer unsubscribeDestination(email)

Unsubscribe destination by email

Returns integer unsubscribed flag: 1 - true, 0 - false. Unique email required.

### Example
```java
// Import classes:
//import ru.rencredit.volkov.reminder.invoker.ApiException;
//import ru.rencredit.volkov.reminder.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String email = "example@rencredit.ru"; // String | Unique email for destination
try {
    Integer result = apiInstance.unsubscribeDestination(email);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#unsubscribeDestination");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| Unique email for destination |

### Return type

**Integer**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

