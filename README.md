# Destination Reminder
Spring Boot приложение, реализующее функционал отправки письма с содержанием «Пора пить чай!» по списку адресатов 
каждый рабочий день в 17:00.

## Быстрый старт

### Команда для запуска кластера:
```bash
sudo docker-compose up
```

### Команда для завершения работы кластера:
```bash
sudo docker-compose stop
```

### Команда для сборки приложения:
```bash
mvn clean install
```

### Команда для тестирования приложения:
```bash
mvn test
```
Перед началом тестирования необходимо выбрать профиль тестирования. 
По-умолчанию выбран профиль игнорирования тестов. 

### Команда для запуска discovery-сервиса:
```bash
java -jar ./reminder-service-discovery/target/reminder-discovery-service.jar
```

### Команда для запуска mvc-сервера:
```bash
java -jar ./reminder-server/target/reminder-server.jar
```

### Команда для запуска proxy-mvc-сервиса:
```bash
java -jar ./reminder-service-proxy/target/reminder-proxy-service.jar
```

### Команда для запуска rest-mvc-клиента:
```bash
java -jar ./reminder-rest-webapp/target/reminder-rest-webapp.jar
```

### Команда для запуска flux-сервера:
```bash
java -jar ./reminder-server-flux/target/reminder-server-flux.jar
```

### Команда для запуска proxy-flux-сервиса:
```bash
java -jar ./reminder-service-flux-proxy/target/reminder-proxy-flux-service.jar
```

### Команда для запуска rest-flux-клиента:
```bash
java -jar ./reminder-rest-flux-webapp/target/reminder-rest-flux-webapp.jar
```

### Команда для запуска websocket-клиента:
```bash
java -jar ./reminder-websocket-webapp/target/reminder-websocket-webapp.jar
```

Сервис `reminder-service-discovery` будет доступно на порту 8761: [http://localhost:8761](http://localhost:8761).

Сервис `reminder-server` будет доступно на порту 8084: [http://localhost:8084](http://localhost:8084).

Сервис `reminder-service-proxy` будет доступно на порту 8080: [http://localhost:8080](http://localhost:8080).

Сервис `reminder-rest-webapp` будет доступно на порту 8086: [http://localhost:8086](http://localhost:8086).

Сервис `reminder-server-flux` будет доступно на порту 8094: [http://localhost:8094](http://localhost:8094).

Сервис `reminder-service-flux-proxy` будет доступно на порту 8080: [http://localhost:8080](http://localhost:8080).

Сервис `reminder-rest-flux-webapp` будет доступно на порту 8096: [http://localhost:8096](http://localhost:8096).

Сервис `reminder-websocket-webapp` будет доступно на порту 8098: [http://localhost:8098](http://localhost:8098).

## МКС `reminder-rest-webapp-swagger` предназначен для генерации исходного кода по swagger-контракту из ресурсной директории модуля `reminder-server`. 
Для генерации используется `swagger-codegen-maven-plugin` v2.4.18.

Чтобы выполнить генерацию кода, необходимо:
1. Выбрать профиль `Generate-rest-web-client`;
2. Выполнить команду `mvn clean install`.

## МКС `reminder-rest-flux-webapp-swagger` предназначен для генерации исходного кода по swagger-контракту из ресурсной директории модуля `reminder-server-flux`. 
Для генерации используется `swagger-codegen-maven-plugin` v2.4.18.

Чтобы выполнить генерацию кода, необходимо:
1. Выбрать профиль `Generate-rest-web-flux-client`;
2. Выполнить команду `mvn clean install`.

## Производственный календарь

Отправка напоминаний происходит в соответствии с 
[производственным календарём России](https://data.gov.ru/opendata/7708660670-proizvcalendar), который находится в 
ресурсах приложений `reminder-server` и `reminder-server-flux`. В 
заданное время будет произведена проверка, является ли этот день рабочим в России, и если это так напоминание будет 
отправлено.

Если календарь скачать не удалось, то будет использован календарь из ресурсов сервиса.
Функция скачивания реализована, но не запущена по-умолчанию из-за падения проекта при подключении к proxy.