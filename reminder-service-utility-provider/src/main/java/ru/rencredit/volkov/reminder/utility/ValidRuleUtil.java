package ru.rencredit.volkov.reminder.utility;

import io.micrometer.core.lang.Nullable;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Objects;

@Slf4j
@UtilityClass
@SuppressWarnings("unused")
public class ValidRuleUtil {

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final String aString) {
        log.info("is null or empty {}", aString);
        return !StringUtils.hasText(aString);
    }

    @Contract("null -> true")
    public boolean isNullOrEmpty(@Nullable final String... aStrings) {
        log.info("is null or empty elements {}",
                Arrays.stream(!Objects.isNull(aStrings)
                        ? aStrings
                        : new String[0]).count()
        );
        return Objects.isNull(aStrings) || aStrings.length < 1;
    }

}