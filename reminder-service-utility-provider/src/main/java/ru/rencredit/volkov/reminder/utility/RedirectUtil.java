package ru.rencredit.volkov.reminder.utility;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

@UtilityClass
@SuppressWarnings("unused")
public class RedirectUtil {

    @NotNull
    public String redirect() {
        return redirect(null);
    }

    @NotNull
    public String redirect(@Nullable final String to) {
        return StringUtils.join(
                "redirect:/",
                !Objects.isNull(to) ? to : ""
        );
    }

}