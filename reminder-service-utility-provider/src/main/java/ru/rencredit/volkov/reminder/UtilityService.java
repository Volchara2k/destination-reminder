package ru.rencredit.volkov.reminder;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UtilityService {

	public static void main(@Nullable final String[] args) {
		SpringApplication.run(UtilityService.class, args);
	}

}