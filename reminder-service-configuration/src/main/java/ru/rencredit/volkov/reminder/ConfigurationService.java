package ru.rencredit.volkov.reminder;

import org.jetbrains.annotations.Nullable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigurationService {

    public static void main(@Nullable final String[] args) {
        SpringApplication.run(ConfigurationService.class, args);
    }

}