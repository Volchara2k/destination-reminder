package ru.rencredit.volkov.reminder.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfiguration {

    @Bean
    @NotNull
    public RestTemplate restTemplate(@NotNull final RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder.build();
    }

}