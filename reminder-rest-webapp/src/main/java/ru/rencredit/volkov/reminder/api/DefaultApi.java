package ru.rencredit.volkov.reminder.api;

import ru.rencredit.volkov.reminder.invoker.ApiClient;

import ru.rencredit.volkov.reminder.domain.AuthenticationRequestDTO;
import ru.rencredit.volkov.reminder.domain.CreateUserRequestDTO;
import ru.rencredit.volkov.reminder.domain.Destination;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;


@Component("ru.rencredit.volkov.reminder.api.DefaultApi")
public class DefaultApi {
    private ApiClient apiClient;

    public DefaultApi() {
        this(new ApiClient());
    }

    @Autowired
    public DefaultApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Get all destinations
     * Returns a complete list of destination details.
     * <p><b>200</b> - successful operation
     * @return List&lt;Destination&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<Destination> getAllDestination() throws RestClientException {
        return getAllDestinationWithHttpInfo().getBody();
    }

    /**
     * Get all destinations
     * Returns a complete list of destination details.
     * <p><b>200</b> - successful operation
     * @return ResponseEntity&lt;List&lt;Destination&gt;&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<List<Destination>> getAllDestinationWithHttpInfo() throws RestClientException {
        Object postBody = null;
        
        String path = UriComponentsBuilder.fromPath("/api/v1/destinations").build().toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<Destination>> returnType = new ParameterizedTypeReference<List<Destination>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get all destination emails
     * Returns a complete list of destination emails details.
     * <p><b>200</b> - successful operation
     * @return List&lt;String&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public List<String> getAllDestinationsEmail() throws RestClientException {
        return getAllDestinationsEmailWithHttpInfo().getBody();
    }

    /**
     * Get all destination emails
     * Returns a complete list of destination emails details.
     * <p><b>200</b> - successful operation
     * @return ResponseEntity&lt;List&lt;String&gt;&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<List<String>> getAllDestinationsEmailWithHttpInfo() throws RestClientException {
        Object postBody = null;
        
        String path = UriComponentsBuilder.fromPath("/api/v1/destinations/emails").build().toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<List<String>> returnType = new ParameterizedTypeReference<List<String>>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Get destination by unique ID
     * Return destination by unique UD. Unique ID required.
     * <p><b>200</b> - successful operation
     * @param id Unique ID of destination (required)
     * @return Destination
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Destination getDestinationById(String id) throws RestClientException {
        return getDestinationByIdWithHttpInfo(id).getBody();
    }

    /**
     * Get destination by unique ID
     * Return destination by unique UD. Unique ID required.
     * <p><b>200</b> - successful operation
     * @param id Unique ID of destination (required)
     * @return ResponseEntity&lt;Destination&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Destination> getDestinationByIdWithHttpInfo(String id) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'id' is set
        if (id == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'id' when calling getDestinationById");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("id", id);
        String path = UriComponentsBuilder.fromPath("/api/v1/destination/{id}").buildAndExpand(uriVariables).toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Destination> returnType = new ParameterizedTypeReference<Destination>() {};
        return apiClient.invokeAPI(path, HttpMethod.GET, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Sign in process
     * User sign in process. Authentication request required.
     * <p><b>0</b> - successful operation
     * @param authenticationRequest Authentication user request (required)
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public void login(AuthenticationRequestDTO authenticationRequest) throws RestClientException {
        loginWithHttpInfo(authenticationRequest);
    }

    /**
     * Sign in process
     * User sign in process. Authentication request required.
     * <p><b>0</b> - successful operation
     * @param authenticationRequest Authentication user request (required)
     * @return ResponseEntity&lt;Void&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Void> loginWithHttpInfo(AuthenticationRequestDTO authenticationRequest) throws RestClientException {
        Object postBody = authenticationRequest;
        
        // verify the required parameter 'authenticationRequest' is set
        if (authenticationRequest == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'authenticationRequest' when calling login");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/v1/login").build().toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Void> returnType = new ParameterizedTypeReference<Void>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Sign up process
     * User sign up process. Create user request required.
     * <p><b>0</b> - successful operation
     * @param createUserRequest Create user request (required)
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public void signUp(CreateUserRequestDTO createUserRequest) throws RestClientException {
        signUpWithHttpInfo(createUserRequest);
    }

    /**
     * Sign up process
     * User sign up process. Create user request required.
     * <p><b>0</b> - successful operation
     * @param createUserRequest Create user request (required)
     * @return ResponseEntity&lt;Void&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Void> signUpWithHttpInfo(CreateUserRequestDTO createUserRequest) throws RestClientException {
        Object postBody = createUserRequest;
        
        // verify the required parameter 'createUserRequest' is set
        if (createUserRequest == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'createUserRequest' when calling signUp");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/v1/sign-up").build().toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Void> returnType = new ParameterizedTypeReference<Void>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Subscribe destination
     * Returns subscribed destination. Created destination required.
     * <p><b>200</b> - successful operation
     * @param email Unique email for destination (required)
     * @return Destination
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Destination subscribeDestination(String email) throws RestClientException {
        return subscribeDestinationWithHttpInfo(email).getBody();
    }

    /**
     * Subscribe destination
     * Returns subscribed destination. Created destination required.
     * <p><b>200</b> - successful operation
     * @param email Unique email for destination (required)
     * @return ResponseEntity&lt;Destination&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Destination> subscribeDestinationWithHttpInfo(String email) throws RestClientException {
        Object postBody = email;
        
        // verify the required parameter 'email' is set
        if (email == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'email' when calling subscribeDestination");
        }
        
        String path = UriComponentsBuilder.fromPath("/api/v1/destination/subscribe").build().toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Destination> returnType = new ParameterizedTypeReference<Destination>() {};
        return apiClient.invokeAPI(path, HttpMethod.POST, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
    /**
     * Unsubscribe destination by email
     * Returns integer unsubscribed flag: 1 - true, 0 - false. Unique email required.
     * <p><b>200</b> - successful operation
     * @param email Unique email for destination (required)
     * @return Integer
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public Integer unsubscribeDestination(String email) throws RestClientException {
        return unsubscribeDestinationWithHttpInfo(email).getBody();
    }

    /**
     * Unsubscribe destination by email
     * Returns integer unsubscribed flag: 1 - true, 0 - false. Unique email required.
     * <p><b>200</b> - successful operation
     * @param email Unique email for destination (required)
     * @return ResponseEntity&lt;Integer&gt;
     * @throws RestClientException if an error occurs while attempting to invoke the API
     */
    public ResponseEntity<Integer> unsubscribeDestinationWithHttpInfo(String email) throws RestClientException {
        Object postBody = null;
        
        // verify the required parameter 'email' is set
        if (email == null) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Missing the required parameter 'email' when calling unsubscribeDestination");
        }
        
        // create path and map variables
        final Map<String, Object> uriVariables = new HashMap<String, Object>();
        uriVariables.put("email", email);
        String path = UriComponentsBuilder.fromPath("/api/v1/destination/unsubscribe/{email}").buildAndExpand(uriVariables).toUriString();

        final MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
        final HttpHeaders headerParams = new HttpHeaders();
        final MultiValueMap<String, Object> formParams = new LinkedMultiValueMap<String, Object>();

        final String[] accepts = { 
            "application/json"
        };
        final List<MediaType> accept = apiClient.selectHeaderAccept(accepts);
        final String[] contentTypes = { };
        final MediaType contentType = apiClient.selectHeaderContentType(contentTypes);

        String[] authNames = new String[] {  };

        ParameterizedTypeReference<Integer> returnType = new ParameterizedTypeReference<Integer>() {};
        return apiClient.invokeAPI(path, HttpMethod.DELETE, queryParams, postBody, headerParams, formParams, accept, contentType, authNames, returnType);
    }
}
