package ru.rencredit.volkov.reminder.invoker.auth;

public enum OAuthFlow {
    accessCode, implicit, password, application
}